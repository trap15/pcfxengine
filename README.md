PCFX-Engine
========
***PCFX-Engine*** is a NEC PC-Engine emulator for the NEC PC-FX. It is currently a work in progress. There will be no support for this software.

Links
========
- <http://daifukkat.su/> - My website
- <https://bitbucket.org/trap15/pcfxengine> - This repository

Greetz
========
- Charles MacDonald
- Ryphecha
- \#raidenii - Forever impossible
- Beer and coffee

Licensing
========
All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2014 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.