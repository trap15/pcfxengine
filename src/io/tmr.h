#ifndef FXENG_IO_TMR_H_
#define FXENG_IO_TMR_H_

void tmr_init(void);
void tmr_reset(void);

u8 tmr_io_r(u16 adr);
void tmr_io_w(u16 adr, u8 dat);

#endif
