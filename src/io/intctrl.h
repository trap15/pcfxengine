#ifndef FXENG_IO_INTCTRL_H_
#define FXENG_IO_INTCTRL_H_

void intctrl_init(void);
void intctrl_reset(void);

u8 intctrl_io_r(u16 adr);
void intctrl_io_w(u16 adr, u8 dat);

#define IRQ_TIMER (1<<2)
#define IRQ_IRQ1 (1<<1)
#define IRQ_IRQ2 (1<<0)

#endif
