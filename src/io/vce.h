#ifndef FXENG_IO_VCE_H_
#define FXENG_IO_VCE_H_

void vce_init(void);
void vce_reset(void);

u8 vce_io_r(u16 adr);
void vce_io_w(u16 adr, u8 dat);

#endif
