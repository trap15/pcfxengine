#ifndef FXENG_IO_EXT1_H_
#define FXENG_IO_EXT1_H_

void ext1_init(void);
void ext1_reset(void);

u8 ext1_io_r(u16 adr);
void ext1_io_w(u16 adr, u8 dat);

#endif
