#ifndef FXENG_IO_IOPRT_H_
#define FXENG_IO_IOPRT_H_

void ioprt_init(void);
void ioprt_reset(void);

u8 ioprt_io_r(u16 adr);
void ioprt_io_w(u16 adr, u8 dat);

#endif
