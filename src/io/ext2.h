#ifndef FXENG_IO_EXT2_H_
#define FXENG_IO_EXT2_H_

void ext2_init(void);
void ext2_reset(void);

u8 ext2_io_r(u16 adr);
void ext2_io_w(u16 adr, u8 dat);

#endif
