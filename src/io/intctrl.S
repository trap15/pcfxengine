	.section .text
	.align	4

	.global	_intctrl_init
	.global	_intctrl_reset
	.global	_intctrl_io_r
	.global	_intctrl_io_w

/* void intctrl_init(void); */
_intctrl_init:
	jmp	[lp]

/* void intctrl_reset(void); */
_intctrl_reset:
	movhi	hi(_g_cpu), r0, r10
	movea	lo(_g_cpu), r10, r10
	mov	7, r11 /* IRQ_TIMER|IRQ_IRQ1|IRQ_IRQ2 */
	st.b	r11, 0x10[r10] /* g_cpu.irq_mask */
	st.b	r0, 0x11[r10] /* g_cpu.irq_pend */
	jmp	[lp]

/* u8 intctrl_io_r(u16 adr); */
_intctrl_io_r:
	movhi	hi(_g_io_buf), r0, r1
	shl	31, r6
	bnc	1f
	movhi	hi(_g_cpu), r0, r10
	ld.b	lo(_g_io_buf)[r1], r11
	movea	lo(_g_cpu), r10, r10
	shl	1, r6
	bnc	2f
/* 0x3: Pending */
	andi	0xF8, r11, r11
	ld.b	0x11[r10], r10 /* g_cpu.irq_pend */
	or	r11, r10
	st.b	r10, lo(_g_io_buf)[r1]
	jmp	[lp]
/* 0x2: Enables */
2:
	andi	0xF8, r11, r11
	ld.b	0x10[r10], r10 /* g_cpu.irq_mask */
	or	r11, r10
	st.b	r10, lo(_g_io_buf)[r1]
	jmp	[lp]
/* 0x0,0x01: Unmapped, just returns io_buf */
1:
	ld.b	lo(_g_io_buf)[r1], r10
	jmp	[lp]

/* void intctrl_io_w(u16 adr, u8 dat); */
_intctrl_io_w:
	movhi	hi(_g_io_buf), r0, r1
	st.b	r7, lo(_g_io_buf)[r1]
	shl	31, r6
	bnc	1f
	movhi	hi(_g_cpu), r0, r1
	movea	lo(_g_cpu), r1, r1
	shl	1, r6
	bnc	2f
/* 0x3: Pending */
	ld.b	0x11[r1], r10 /* g_cpu.irq_pend */
	andi	0x3, r10, r10
	st.b	r10, 0x11[r1] /* g_cpu.irq_pend */
	jmp	[lp]
/* 0x2: Enables */
2:
	andi	0x7, r7, r10
	st.b	r10, 0x10[r1] /* g_cpu.irq_mask */
/* 0x0,0x01: Unmapped, just stores to io_buf */
1:
	jmp	[lp]
