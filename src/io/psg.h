#ifndef FXENG_IO_PSG_H_
#define FXENG_IO_PSG_H_

void psg_init(void);
void psg_reset(void);

u8 psg_io_r(u16 adr);
void psg_io_w(u16 adr, u8 dat);

#endif
