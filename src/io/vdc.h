#ifndef FXENG_IO_VDC_H_
#define FXENG_IO_VDC_H_

void vdc_init(void);
void vdc_reset(void);

u8 vdc_io_r(u16 adr);
void vdc_io_w(u16 adr, u8 dat);

#endif
