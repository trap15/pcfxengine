#ifndef FXENG_TOP_H_
#define FXENG_TOP_H_

#include <eris/types.h>
#include <eris/std.h>
#include <eris/v810.h>

#include <eris/pad.h>
#include <eris/timer.h>
#include <eris/tetsu.h>
#include <eris/king.h>
#include <eris/low/7up.h>
#include <eris/romfont.h>

#define NULL 0
typedef uint32_t size_t;

#include "emu.h"
#include "mem.h"
#include "cpu/cpu.h"
#include "rom/rom.h"
#include "rom/hes.h"

#include "io/vdc.h"
#include "io/vce.h"
#include "io/psg.h"
#include "io/tmr.h"
#include "io/ioprt.h"
#include "io/intctrl.h"
#include "io/ext1.h"
#include "io/ext2.h"

#include "ui/ui.h"

#define KING_PAL_BASE 256

#define VCE_BASE 0x300

#define LVL_TIMER 9
#define LVL_PAD   11
#define LVL_7UPA  12
#define LVL_KING  13
#define LVL_7UPB  14

#define DBGCODE_CYCLE 0xFF
#define DBGCODE_IRQ 0x10
#define DBGCODE_DFLAG 0xA5
//#define DBGCODE_FIXR 0x5A

#define BRKLVL 1

#if BRKLVL <= 0
#define dbgbrk0 dbgbreak
#else
#define dbgbrk0(X...)
#endif

#if BRKLVL <= 1
#define dbgbrk1 dbgbreak
#else
#define dbgbrk1(X...)
#endif

#if BRKLVL <= 2
#define dbgbrk2 dbgbreak
#else
#define dbgbrk2(X...)
#endif

#if BRKLVL <= 3
#define dbgbrk3 dbgbreak
#else
#define dbgbrk3(X...)
#endif

extern int g_running;

#endif
