#include "top.h"

extern u8 *g_cpurom;
extern size_t g_romsize;

extern u8 g_tmprom[];
extern u32 g_tmprom_size;

void hes_load(void)
{
  g_cpurom = g_tmprom;
  g_romsize = g_tmprom_size;
}

void hes_set(void)
{
  int i, l;
  for(i = 0; i < 8; i++) {
    mem_setmpr(i, mem_r8_hw(8+i));
  }

  /* VDC setup */
  for(l = 0; l < 2; l++) {
    mem_w8_hw(0x1FE00E, l); /* VDC select? */
    for(i = 0; i < 0x20; i++) {
      mem_w8_hw(0x1FE000, i);
      mem_w8_hw(0x1FE010, i);
      mem_w8_hw(0x1FE002, 0x00);
      mem_w8_hw(0x1FE003, 0x00);
    }
    mem_w8_hw(0x1FE000, 0x0A);
    mem_w8_hw(0x1FE002, 0x02);
    mem_w8_hw(0x1FE003, 0x02);
    mem_w8_hw(0x1FE000, 0x0B);
    mem_w8_hw(0x1FE002, 0x1F);
    mem_w8_hw(0x1FE003, 0x04);
    mem_w8_hw(0x1FE000, 0x0C);
    mem_w8_hw(0x1FE002, 0x02);
    mem_w8_hw(0x1FE003, 0x0D);
    mem_w8_hw(0x1FE000, 0x0D);
    mem_w8_hw(0x1FE002, 0xEF);
    mem_w8_hw(0x1FE003, 0x00);
    mem_w8_hw(0x1FE000, 0x0E);
    mem_w8_hw(0x1FE002, 0x04);
  }
  /* VCE setup */
  mem_w8_hw(0x1FE400, 0x04);
}

void hes_play(int id)
{
  g_cpu.a = id;
  cpu_makecall(mem_r8_hw(6) | (mem_r8_hw(7) << 8));
}
