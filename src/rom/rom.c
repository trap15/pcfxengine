#include "top.h"

extern u8 *g_cpurom;
extern size_t g_romsize;

extern u8 g_tmprom[];
extern u32 g_tmprom_size;

void rom_load(void)
{
  g_cpurom = g_tmprom;
  g_romsize = g_tmprom_size;
}
