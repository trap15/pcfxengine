#ifndef FXENG_MEM_H_
#define FXENG_MEM_H_

typedef u8 (*MmioR8)(u16 adr);
typedef void (*MmioW8)(u16 adr, u8 dat);

void mem_init(void);
void mem_reset(void);

u8 mem_fetch8(u16 adr);
u8 mem_r8(u16 adr);
u8 mem_r8_hw(u32 adr);
void mem_w8(u16 adr, u8 dat);
void mem_w8_hw(u32 adr, u8 dat);

void mem_setmpr(int mpr, int bank);
u8 mem_getmpr(int mpr);

extern u8 g_io_buf;

extern u8 *g_hwmap[0x100]; /* Every 0x2000 */
extern u8 *g_mprmap[0x8]; /* Every 0x2000 */
extern u8 g_mprmap_val[0x8]; /* Every 0x2000 */
extern MmioR8 g_iormap[0x8]; /* Every 0x400 */
extern MmioW8 g_iowmap[0x8]; /* Every 0x400 */

#endif
