#include "top.h"

void ui_king_setup(void)
{
	int i;
	u16 microprog[16];
	eris_king_init();

	eris_king_set_bg_prio(KING_BGPRIO_3, KING_BGPRIO_HIDE, KING_BGPRIO_HIDE, KING_BGPRIO_HIDE, 0);
	eris_king_set_bg_mode(KING_BGMODE_4_PAL, 0, 0, 0);
	eris_king_set_kram_pages(0, 0, 0, 0);

	for(i = 0; i < 16; i++) {
		microprog[i] = KING_CODE_NOP;
	}

	microprog[0] = KING_CODE_BG0_CG_0;
	eris_king_disable_microprogram();
	eris_king_write_microprogram(microprog, 0, 16);
	eris_king_enable_microprogram();

	eris_king_set_bat_cg_addr(KING_BG0, 0, 0);
	eris_king_set_bat_cg_addr(KING_BG0SUB, 0, 0);
	eris_king_set_scroll(KING_BG0, 0, 0);
	eris_king_set_bg_size(KING_BG0, KING_BGSIZE_256, KING_BGSIZE_256, KING_BGSIZE_256, KING_BGSIZE_256);

	eris_king_set_kram_read(0, 1);
	eris_king_set_kram_write(0, 1);
	// Clear BG0's RAM
	for(i = 0; i < 0x1E00; i++) {
		eris_king_kram_write(0);
	}
}

void ui_vdc_irq_handler(void)
{
  in16(0x500);
  ui_update();
}

void ui_7upb_setup(void)
{
  irq_set_handler(LVL_7UPB, ui_vdc_irq_handler);
  irq_level_enable(LVL_7UPB);

  eris_low_sup_init(1);
  eris_low_sup_set_video_mode(1, 2,2,4,0x1F, 0x0F,0x02,0x00,0xEF);
  eris_low_sup_set_control(1, 0,0,0);
  eris_low_sup_set_interrupts(1, 1,0,0,0);
}

void ui_tetsu_set_mode(int bg0disp)
{
  int mode;
  out16(VCE_BASE+0, 0x00);
  mode = in16(VCE_BASE+4);
  if(bg0disp)
    mode |= 1 << 10;
  else
    mode &= ~(1 << 10);
  out16(VCE_BASE+4, mode);
}

void ui_tetsu_set_pal(void)
{
  eris_tetsu_set_palette(KING_PAL_BASE+0, 0x0088);
	eris_tetsu_set_palette(KING_PAL_BASE+1, 0xE088);
	eris_tetsu_set_palette(KING_PAL_BASE+2, 0xE0F0);
	eris_tetsu_set_palette(KING_PAL_BASE+3, 0x602C);
	eris_tetsu_set_palette(KING_PAL_BASE+4, 0x5080);
	eris_tetsu_set_palette(KING_PAL_BASE+5, 0xC422);
	eris_tetsu_set_palette(KING_PAL_BASE+6, 0x9999);
	eris_tetsu_set_palette(KING_PAL_BASE+7, 0x1234);
}

void ui_tetsu_setup(void)
{
  ui_tetsu_set_mode(0);
}

void printch(u32 sjis, u32 kram, int tall)
{
	u16 px;
	int x, y;
	u8* glyph = eris_romfont_get(sjis, tall ? ROMFONT_ANK_8x16 : ROMFONT_ANK_8x8);
	for(y = 0; y < (tall ? 16 : 8); y++) {
		px = 0;
		for(x = 0; x < 8; x++) {
			if((glyph[y] >> x) & 1) {
				px |= 1 << (x << 1);
			}
		}
		eris_king_set_kram_write(kram + (y << 5), 1);
		eris_king_kram_write(px);
	}
}
void printstr(char* str, int x, int y, int tall)
{
	int i;
	u32 kram = x + (y << 5);
	int len = strlen8(str);
	for(i = 0; i < len; i++) {
		printch(str[i], kram + i, tall);
	}
}
void printhex(u32 v, int bytes, int x, int y, int tall)
{
	int i;
  u32 c;
	u32 kram = x + (y << 5);
	for(i = 0; i < bytes; i++) {
    c = (v >> ((bytes-i-1) << 2)) & 0xF;
    if(c < 10) {
      c = '0' + c;
    }else{
      c = 'A' + c - 10;
    }
		printch(c, kram + i, tall);
	}
}

void ui_setup(void);
/* Setup KING and stuff so we can overlay things.
 * Also setup a 7up vsync interrupt so that we can update consistently without
 * breaking the emu loop.
 */
void ui_init(void)
{
  /* Setup KING */
  ui_king_setup();
  /* Setup VCE */
  ui_tetsu_setup();
  /* Setup 7up-B */
  ui_7upb_setup();

  ui_setup();
}

static u32 ui_oldpad;
static u32 ui_dbgdisp;

void ui_setup(void)
{
  printstr("DEM DEBUGGINZ", 9,0x10, 1);
  ui_oldpad = 0;
  ui_dbgdisp = 0;
  g_running = 0;
}

/* Debugger controls:
 *
 * 5+1: Run one instruction
 * 5+2: Run to break addr
 * 5+3: Run while held
 * 6+1: Save IMSK
 * 6+2: Restore IMSK
 * 6+3: Mask IRQ1
 * 6+4: Mask TIQ
 * 6+5: Unmask all
 * Run+Sel: Reset
 *
 */

// ISSUE in E340
 
static u8 saved_imsk;

void ui_dbg_ctrl(u32 pad, u32 paddown, u32 padup)
{
  if(pad & (1<<4)) {
    if(paddown & (1<<0)) {
      cpu_run_once();
    }
    if(paddown & (1<<1)) {
      while(g_cpu.pc != 0xE340)
        cpu_run_once();
    }
    if(pad & (1<<2)) {
      cpu_run_once();
    }
  }
  if(pad & (1<<5)) {
    if(paddown & (1<<0)) {
      saved_imsk = g_cpu.irq_mask;
    }
    if(paddown & (1<<1)) {
      g_cpu.irq_mask = saved_imsk;
    }
    if(paddown & (1<<2)) {
      g_cpu.irq_mask |= 2;
    }
    if(paddown & (1<<3)) {
      g_cpu.irq_mask |= 4;
    }
    if(paddown & (1<<4)) {
      g_cpu.irq_mask &= 1;
    }
  }
  if((pad & (3 << 6)) == (3 << 6))
    emu_reset();
}

#define DBGREG_XOFF 1
#define DBGREG_XSHF 3
#define DBGREG_YOFF 0x20
#define DBGREG_YSHF 3
#define DBGREG_VXOFF 5

typedef struct {
  u32 x, y;
  char* name;
  void* ptr;
  int size;
} DbgInfo;

static DbgInfo dbginfo[] = {
  { .x = 0, .y = 0, .name = "   A", .ptr = &g_cpu.a, .size = 2 },
  { .x = 0, .y = 1, .name = "   X", .ptr = &g_cpu.x, .size = 2 },
  { .x = 0, .y = 2, .name = "   Y", .ptr = &g_cpu.y, .size = 2 },
  { .x = 0, .y = 3, .name = "   P", .ptr = &g_cpu.p, .size = 2 },
  { .x = 0, .y = 4, .name = "   S", .ptr = &g_cpu.s, .size = 2 },
  { .x = 0, .y = 5, .name = "  PC", .ptr = &g_cpu.pc, .size = 4 },
  { .x = 0, .y = 6, .name = "IPND", .ptr = &g_cpu.irq_pend, .size = 1 },
  { .x = 0, .y = 7, .name = "IMSK", .ptr = &g_cpu.irq_mask, .size = 1 },
  { .x = 0, .y = 8, .name = " SPD", .ptr = &g_cpu.speed, .size = 1 },
  { 0,0,NULL,NULL,0 },
};
void ui_dbg_update(void)
{
  u32 val = 0;
  DbgInfo *ptr;
  for(ptr = dbginfo; ptr->ptr != NULL; ptr++) {
    switch(ptr->size) {
      case 1: case 2: val = *((u8*)ptr->ptr); break;
      case 3: case 4: val = *((u16*)ptr->ptr); break;
      case 5: case 6: case 7: case 8: val = *((u32*)ptr->ptr); break;
    }
    printstr(ptr->name, DBGREG_XOFF + (ptr->x << DBGREG_XSHF),
                        DBGREG_YOFF + (ptr->y << DBGREG_YSHF), 0);
    printhex(val, ptr->size,
                  DBGREG_VXOFF + DBGREG_XOFF + (ptr->x << DBGREG_XSHF),
                  DBGREG_YOFF + (ptr->y << DBGREG_YSHF), 0);
  }
}

void ui_update(void)
{
  u32 pad = eris_pad_read(0);
  u32 paddown = (ui_oldpad ^ pad) & pad;
  u32 padup = (ui_oldpad ^ pad) & ui_oldpad;

  /* Mode 2 shows debug display. mode 1 implies mode 2 */
  u32 dbgdisp = pad & ((1 << 12) | (1 << 14));

  /* Mode 1 switch enables debug controls and pauses emulation */
  if(pad & (1 << 12)) { /* is on */
    g_running = 0;
  }else {
    g_running = 1;
  }

  if(pad & (1 << 12)) {
    ui_dbg_ctrl(pad, paddown, padup);
  }

  if(!ui_dbgdisp && dbgdisp) { /* turn on */
    ui_tetsu_set_mode(1);
    ui_tetsu_set_pal();
  }else if(ui_dbgdisp && !dbgdisp) { /* turn off */
    ui_tetsu_set_mode(0);
  }

  if(dbgdisp) {
    ui_dbg_update();
  }
  ui_dbgdisp = dbgdisp;
  ui_oldpad = pad;
}
