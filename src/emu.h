#ifndef FXENG_EMU_H_
#define FXENG_EMU_H_

void emu_init(void);
void emu_reset(void);
void emu_run(void);

#endif
