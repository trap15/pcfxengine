#include "top.h"

#define EMURAM_SIZE 0x8000

u8 *g_cpurom;
size_t g_romsize;

u8 g_cpuram[EMURAM_SIZE];
u8 *g_hwmap[0x100]; /* Every 0x2000 */
u8 *g_mprmap[0x8]; /* Every 0x2000 */
u8 g_mprmap_val[0x8]; /* Every 0x2000 */
MmioR8 g_iormap[0x8]; /* Every 0x400 */
MmioW8 g_iowmap[0x8]; /* Every 0x400 */

u8 g_io_buf;

void mem_init(void)
{
  mem_reset();
}

void mem_reset(void)
{
  int i, l;
  u8 *ptr;
  memset32(g_cpuram, 0, EMURAM_SIZE/4);

  g_io_buf = 0x00; /* idk lol */

  /* setup the hw mapping */
  for(i = 0; i < 0x100; i++) {
    if(i < 0xF8) {
      ptr = g_cpurom + ((i * 0x2000) % g_romsize);
    }else if(i < 0xFC) {
      l = i - 0xF8;
      ptr = g_cpuram + ((l * 0x2000) % EMURAM_SIZE);
    }else if(i < 0xFF) { /* Unmapped? */
      l = i - 0xF8;
      ptr = g_cpuram + ((l * 0x2000) % EMURAM_SIZE);
    }else{ /* I/O, Segment FF */
      ptr = NULL;
    }
    g_hwmap[i] = ptr;
  }

  /* I/O mapping */
  g_iormap[0] = vdc_io_r;
  g_iormap[1] = vce_io_r;
  g_iormap[2] = psg_io_r;
  g_iormap[3] = tmr_io_r;
  g_iormap[4] = ioprt_io_r;
  g_iormap[5] = intctrl_io_r;
  g_iormap[6] = ext1_io_r;
  g_iormap[7] = ext2_io_r;
  g_iowmap[0] = vdc_io_w;
  g_iowmap[1] = vce_io_w;
  g_iowmap[2] = psg_io_w;
  g_iowmap[3] = tmr_io_w;
  g_iowmap[4] = ioprt_io_w;
  g_iowmap[5] = intctrl_io_w;
  g_iowmap[6] = ext1_io_w;
  g_iowmap[7] = ext2_io_w;

  /* default mpr mapping */
  mem_setmpr(0, 0xFF);
  mem_setmpr(1, 0xF8);
  mem_setmpr(2, 0x00);
  mem_setmpr(3, 0x01);
  mem_setmpr(4, 0x02);
  mem_setmpr(5, 0x03);
  mem_setmpr(6, 0x04);
  mem_setmpr(7, 0x00);
}
