#ifndef FXENG_CPU_H_
#define FXENG_CPU_H_

typedef struct CpuState CpuState;
struct CpuState {
/* +0 */
  s32 cyc_left;

/* +4 */
  u16 pc;
/* +6 */
  u16 pad1;
/* +8 */
  u8 s;
/* +9 */
  u8 pad2_fix1;

/* +A */
  u8 a;
/* +B */
  u8 p;
/* +C */
  u8 x;
/* +D */
  u8 y;

/* +E */
  u8 speed;
/* +F */
  u8 pad3;
/* +10 */
  u8 irq_mask;
/* +11 */
  u8 irq_pend;
};

extern CpuState g_cpu;

void cpu_init(void);
void cpu_reset(void);

/* Force an IRQ */
void cpu_irq(int vec);
/* Force a call */
void cpu_makecall(u16 npc);

/* Things to implement per core */
void cpucore_init(void);
s32 cpu_run(s32 cycles);
void cpu_run_once(void);
/* Push a byte */
void cpu_push(u8 v);
/* Pop a byte */
u8 cpu_pop(void);

#define CPUV_RESET 0xFFFE
#define CPUV_NMI 0xFFFC
#define CPUV_TIQ 0xFFFA
#define CPUV_IRQ1 0xFFF8
#define CPUV_IRQ2 0xFFF6
#define CPUV_BRK CPUV_IRQ2

#define PFLG_C 0x01
#define PFLG_Z 0x02
#define PFLG_I 0x04
#define PFLG_D 0x08
#define PFLG_B 0x10
#define PFLG_T 0x20
#define PFLG_V 0x40
#define PFLG_N 0x80

#endif
