/* TODO: T flag stuff */

#include "top.h"

#include "cpu/cpu.h"
#include "cpu/c/core.h"

CpuState g_cpu;

static s8 cpu_cyc_tbl[256] = {
/*       x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, xA, xB, xC, xD, xE, xF */
/*0x*/  - 8,- 7,- 3,- 4,- 6,- 4,- 6,- 7,- 3,- 2,- 2,-99,- 7,- 5,- 7,- 6,
/*1x*/  - 2,- 7,- 7,- 4,- 6,- 4,- 6,- 7,- 2,- 5,- 2,-99,- 7,- 5,- 7,- 6,
/*2x*/  - 7,- 7,- 3,- 4,- 4,- 4,- 6,- 7,- 4,- 2,- 2,-99,- 5,- 5,- 7,- 6,
/*3x*/  - 2,- 7,- 7,-99,- 4,- 4,- 6,- 7,- 2,- 5,- 2,-99,- 5,- 5,- 7,- 6,
/*4x*/  - 7,- 7,- 3,- 4,- 8,- 4,- 6,- 7,- 3,- 2,- 2,-99,- 4,- 5,- 7,- 6,
/*5x*/  - 2,- 7,- 7,- 5,- 2,- 4,- 6,- 7,- 2,- 5,- 3,-99,-99,- 5,- 7,- 6,
/*6x*/  - 7,- 7,- 2,-99,- 4,- 4,- 6,- 7,- 4,- 2,- 2,-99,- 7,- 5,- 7,- 6,
/*7x*/  - 2,- 7,- 7,-17,- 4,- 4,- 6,- 7,- 2,- 5,- 4,-99,- 7,- 5,- 7,- 6,
/*8x*/  - 2,- 7,- 2,- 7,- 4,- 4,- 4,- 7,- 2,- 2,- 2,-99,- 5,- 5,- 5,- 6,
/*9x*/  - 2,- 7,- 7,- 8,- 4,- 4,- 4,- 7,- 2,- 5,- 2,-99,- 5,- 5,- 5,- 6,
/*Ax*/  - 2,- 7,- 2,- 7,- 4,- 4,- 4,- 7,- 2,- 2,- 2,-99,- 5,- 5,- 5,- 6,
/*Bx*/  - 2,- 7,- 7,- 8,- 4,- 4,- 4,- 7,- 2,- 5,- 2,-99,- 5,- 5,- 5,- 6,
/*Cx*/  - 2,- 7,- 2,-17,- 4,- 4,- 6,- 7,- 2,- 2,- 2,-99,- 5,- 5,- 7,- 6,
/*Dx*/  - 2,- 7,- 7,-17,- 2,- 4,- 6,- 7,- 2,- 5,- 3,-99,-99,- 5,- 7,- 6,
/*Ex*/  - 2,- 7,- 1,-17,- 4,- 4,- 6,- 7,- 2,- 2,- 2,-99,- 5,- 5,- 7,- 6,
/*Fx*/  - 2,- 7,- 7,-17,- 2,- 4,- 6,- 7,- 2,- 5,- 4,-99,-99,- 5,- 7,- 6,
};

static u8 cpu_zn_tbl[256] = {
  PFLG_Z,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
  PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,PFLG_N,
};

void cpucore_init(void)
{
}

#include "cpu/c/prims.inc.c"

void cpu_run_one(u8 insn)
{
  int i;
  s8 disp;
  u32 param = 0;
  u16 adr;
  u8 tst_nn;
  u8 base;
  u32 res;
  u8 tpfx_saved_a = 0;
  u16 blk_src, blk_dst, blk_len;
  switch(insn) {
#include "cpu/c/ops.inc.c"
    default: /* BAD */
      break;
  }
  g_cpu.p &= ~PFLG_T;
_no_T_reset:
  return;
}

void cpu_do_irq(void)
{
  int i;
  int pend = g_cpu.irq_pend & ~g_cpu.irq_mask;
  for(i = 0; i < 3; i++) {
    if(pend & (1 << i)) {
      cpu_irq(CPUV_IRQ2+(i<<1));
      break;
    }
  }
}

s32 cpu_run(s32 cycles)
{
  u8 insn;
  g_cpu.cyc_left += cycles;
  while(g_cpu.cyc_left > 0) {
    dbgbrk0(DBGCODE_CYCLE, g_cpu.pc);
    insn = mem_fetch8(g_cpu.pc++);
    g_cpu.cyc_left -= cpu_cyc_tbl[insn];
    cpu_run_one(insn);
    if(!(g_cpu.p & PFLG_I) && (g_cpu.irq_pend & ~g_cpu.irq_mask)) {
      cpu_do_irq();
    }
  }
  return g_cpu.cyc_left;
}

void cpu_run_once(void)
{
  u8 insn;
  dbgbrk0(DBGCODE_CYCLE, g_cpu.pc);
  insn = mem_fetch8(g_cpu.pc++);
  cpu_run_one(insn);
  if(!(g_cpu.p & PFLG_I) && (g_cpu.irq_pend & ~g_cpu.irq_mask)) {
    cpu_do_irq();
  }
}

/* Push a byte */
void cpu_push(u8 v)
{
  mem_w8(g_cpu.s | 0x2100, v);
  g_cpu.s--;
}

/* Pop a byte */
u8 cpu_pop(void)
{
  g_cpu.s++;
  return mem_r8(g_cpu.s | 0x2100);
}
