/**** adc ****/
case 0x7D: /* abx    */ ARD_ABX(OP_ADC) break;
case 0x72: /* zpind  */ ARD_ZPIND(OP_ADC) break;
case 0x69: /* im     */ ARD_IM(OP_ADC) break;
case 0x65: /* zp     */ ARD_ZP(OP_ADC) break;
case 0x71: /* iy     */ ARD_IY(OP_ADC) break;
case 0x61: /* ix     */ ARD_IX(OP_ADC) break;
case 0x79: /* aby    */ ARD_ABY(OP_ADC) break;
case 0x75: /* zpx    */ ARD_ZPX(OP_ADC) break;
case 0x6D: /* ab     */ ARD_AB(OP_ADC) break;
/**** and ****/
case 0x32: /* zpind  */ ARD_ZPIND(OP_AND) break;
case 0x39: /* aby    */ ARD_ABY(OP_AND) break;
case 0x21: /* ix     */ ARD_IX(OP_AND) break;
case 0x2D: /* ab     */ ARD_AB(OP_AND) break;
case 0x3D: /* abx    */ ARD_ABX(OP_AND) break;
case 0x29: /* im     */ ARD_IM(OP_AND) break;
case 0x35: /* zpx    */ ARD_ZPX(OP_AND) break;
case 0x31: /* iy     */ ARD_IY(OP_AND) break;
case 0x25: /* zp     */ ARD_ZP(OP_AND) break;
/**** asl ****/
case 0x0A: /* a      */ ARW_A(OP_ASL) break;
case 0x06: /* zp     */ ARW_ZP(OP_ASL) break;
case 0x1E: /* abx    */ ARW_ABX(OP_ASL) break;
case 0x0E: /* ab     */ ARW_AB(OP_ASL) break;
case 0x16: /* zpx    */ ARW_ZPX(OP_ASL) break;
/**** bbr0 ****/
case 0x0F: /* zp     */ ARD_ZP(OP_BBR0) break;
/**** bbr1 ****/
case 0x1F: /* zp     */ ARD_ZP(OP_BBR1) break;
/**** bbr2 ****/
case 0x2F: /* zp     */ ARD_ZP(OP_BBR2) break;
/**** bbr3 ****/
case 0x3F: /* zp     */ ARD_ZP(OP_BBR3) break;
/**** bbr4 ****/
case 0x4F: /* zp     */ ARD_ZP(OP_BBR4) break;
/**** bbr5 ****/
case 0x5F: /* zp     */ ARD_ZP(OP_BBR5) break;
/**** bbr6 ****/
case 0x6F: /* zp     */ ARD_ZP(OP_BBR6) break;
/**** bbr7 ****/
case 0x7F: /* zp     */ ARD_ZP(OP_BBR7) break;
/**** bbs0 ****/
case 0x8F: /* zp     */ ARD_ZP(OP_BBS0) break;
/**** bbs1 ****/
case 0x9F: /* zp     */ ARD_ZP(OP_BBS1) break;
/**** bbs2 ****/
case 0xAF: /* zp     */ ARD_ZP(OP_BBS2) break;
/**** bbs3 ****/
case 0xBF: /* zp     */ ARD_ZP(OP_BBS3) break;
/**** bbs4 ****/
case 0xCF: /* zp     */ ARD_ZP(OP_BBS4) break;
/**** bbs5 ****/
case 0xDF: /* zp     */ ARD_ZP(OP_BBS5) break;
/**** bbs6 ****/
case 0xEF: /* zp     */ ARD_ZP(OP_BBS6) break;
/**** bbs7 ****/
case 0xFF: /* zp     */ ARD_ZP(OP_BBS7) break;
/**** bcc ****/
case 0x90: /* rel    */ ARD_REL(OP_BCC) break;
/**** bcs ****/
case 0xB0: /* rel    */ ARD_REL(OP_BCS) break;
/**** beq ****/
case 0xF0: /* rel    */ ARD_REL(OP_BEQ) break;
/**** bit ****/
case 0x24: /* zp     */ ARD_ZP(OP_BIT) break;
case 0x89: /* im     */ ARD_IM(OP_BIT) break;
case 0x3C: /* abx    */ ARD_ABX(OP_BIT) break;
case 0x34: /* zpx    */ ARD_ZPX(OP_BIT) break;
case 0x2C: /* ab     */ ARD_AB(OP_BIT) break;
/**** bmi ****/
case 0x30: /* rel    */ ARD_REL(OP_BMI) break;
/**** bne ****/
case 0xD0: /* rel    */ ARD_REL(OP_BNE) break;
/**** bpl ****/
case 0x10: /* rel    */ ARD_REL(OP_BPL) break;
/**** bra ****/
case 0x80: /* rel    */ ARD_REL(OP_BRA) break;
/**** brk ****/
case 0x00: /* none   */ OP_BRK break;
/**** bsr ****/
case 0x44: /* rel    */ ARD_REL(OP_BSR) break;
/**** bvc ****/
case 0x50: /* rel    */ ARD_REL(OP_BVC) break;
/**** bvs ****/
case 0x70: /* rel    */ ARD_REL(OP_BVS) break;
/**** cla ****/
case 0x62: /* none   */ OP_CLA break;
/**** clc ****/
case 0x18: /* none   */ OP_CLC break;
/**** cld ****/
case 0xD8: /* none   */ OP_CLD break;
/**** cli ****/
case 0x58: /* none   */ OP_CLI break;
/**** clv ****/
case 0xB8: /* none   */ OP_CLV break;
/**** clx ****/
case 0x82: /* none   */ OP_CLX break;
/**** cly ****/
case 0xC2: /* none   */ OP_CLY break;
/**** cmp ****/
case 0xC1: /* ix     */ ARD_IX(OP_CMP) break;
case 0xD2: /* zpind  */ ARD_ZPIND(OP_CMP) break;
case 0xDD: /* abx    */ ARD_ABX(OP_CMP) break;
case 0xD1: /* iy     */ ARD_IY(OP_CMP) break;
case 0xC9: /* im     */ ARD_IM(OP_CMP) break;
case 0xD5: /* zpx    */ ARD_ZPX(OP_CMP) break;
case 0xCD: /* ab     */ ARD_AB(OP_CMP) break;
case 0xD9: /* aby    */ ARD_ABY(OP_CMP) break;
case 0xC5: /* zp     */ ARD_ZP(OP_CMP) break;
/**** cpx ****/
case 0xE0: /* im     */ ARD_IM(OP_CPX) break;
case 0xE4: /* zp     */ ARD_ZP(OP_CPX) break;
case 0xEC: /* ab     */ ARD_AB(OP_CPX) break;
/**** cpy ****/
case 0xCC: /* ab     */ ARD_AB(OP_CPY) break;
case 0xC4: /* zp     */ ARD_ZP(OP_CPY) break;
case 0xC0: /* im     */ ARD_IM(OP_CPY) break;
/**** csh ****/
case 0xD4: /* none   */ OP_CSH break;
/**** csl ****/
case 0x54: /* none   */ OP_CSL break;
/**** dec ****/
case 0xC6: /* zp     */ ARW_ZP(OP_DEC) break;
case 0xD6: /* zpx    */ ARW_ZPX(OP_DEC) break;
case 0x3A: /* a      */ ARW_A(OP_DEC) break;
case 0xDE: /* abx    */ ARW_ABX(OP_DEC) break;
case 0xCE: /* ab     */ ARW_AB(OP_DEC) break;
/**** dex ****/
case 0xCA: /* none   */ OP_DEX break;
/**** dey ****/
case 0x88: /* none   */ OP_DEY break;
/**** eor ****/
case 0x5D: /* abx    */ ARD_ABX(OP_EOR) break;
case 0x51: /* iy     */ ARD_IY(OP_EOR) break;
case 0x4D: /* ab     */ ARD_AB(OP_EOR) break;
case 0x52: /* zpind  */ ARD_ZPIND(OP_EOR) break;
case 0x55: /* zpx    */ ARD_ZPX(OP_EOR) break;
case 0x59: /* aby    */ ARD_ABY(OP_EOR) break;
case 0x49: /* im     */ ARD_IM(OP_EOR) break;
case 0x45: /* zp     */ ARD_ZP(OP_EOR) break;
case 0x41: /* ix     */ ARD_IX(OP_EOR) break;
/**** inc ****/
case 0xF6: /* zpx    */ ARW_ZPX(OP_INC) break;
case 0xFE: /* abx    */ ARW_ABX(OP_INC) break;
case 0xE6: /* zp     */ ARW_ZP(OP_INC) break;
case 0x1A: /* a      */ ARW_A(OP_INC) break;
case 0xEE: /* ab     */ ARW_AB(OP_INC) break;
/**** inx ****/
case 0xE8: /* none   */ OP_INX break;
/**** iny ****/
case 0xC8: /* none   */ OP_INY break;
/**** jmp ****/
case 0x7C: /* abindx */ ARD_ABINDX(OP_JMP) break;
case 0x6C: /* abind  */ ARD_ABIND(OP_JMP) break;
case 0x4C: /* abadr  */ ARD_ABADR(OP_JMP) break;
/**** jsr ****/
case 0x20: /* abadr  */ ARD_ABADR(OP_JSR) break;
/**** lda ****/
case 0xB1: /* iy     */ ARD_IY(OP_LDA) break;
case 0xAD: /* ab     */ ARD_AB(OP_LDA) break;
case 0xB5: /* zpx    */ ARD_ZPX(OP_LDA) break;
case 0xA9: /* im     */ ARD_IM(OP_LDA) break;
case 0xB9: /* aby    */ ARD_ABY(OP_LDA) break;
case 0xA1: /* ix     */ ARD_IX(OP_LDA) break;
case 0xBD: /* abx    */ ARD_ABX(OP_LDA) break;
case 0xB2: /* zpind  */ ARD_ZPIND(OP_LDA) break;
case 0xA5: /* zp     */ ARD_ZP(OP_LDA) break;
/**** ldx ****/
case 0xA2: /* im     */ ARD_IM(OP_LDX) break;
case 0xB6: /* zpy    */ ARD_ZPY(OP_LDX) break;
case 0xA6: /* zp     */ ARD_ZP(OP_LDX) break;
case 0xBE: /* aby    */ ARD_ABY(OP_LDX) break;
case 0xAE: /* ab     */ ARD_AB(OP_LDX) break;
/**** ldy ****/
case 0xAC: /* ab     */ ARD_AB(OP_LDY) break;
case 0xB4: /* zpx    */ ARD_ZPX(OP_LDY) break;
case 0xA4: /* zp     */ ARD_ZP(OP_LDY) break;
case 0xA0: /* im     */ ARD_IM(OP_LDY) break;
case 0xBC: /* abx    */ ARD_ABX(OP_LDY) break;
/**** lsr ****/
case 0x4A: /* a      */ ARW_A(OP_LSR) break;
case 0x4E: /* ab     */ ARW_AB(OP_LSR) break;
case 0x5E: /* abx    */ ARW_ABX(OP_LSR) break;
case 0x46: /* zp     */ ARW_ZP(OP_LSR) break;
case 0x56: /* zpx    */ ARW_ZPX(OP_LSR) break;
/**** nop ****/
case 0xEA: /* none   */ OP_NOP break;
/**** ora ****/
case 0x09: /* im     */ ARD_IM(OP_ORA) break;
case 0x01: /* ix     */ ARD_IX(OP_ORA) break;
case 0x15: /* zpx    */ ARD_ZPX(OP_ORA) break;
case 0x11: /* iy     */ ARD_IY(OP_ORA) break;
case 0x05: /* zp     */ ARD_ZP(OP_ORA) break;
case 0x1D: /* abx    */ ARD_ABX(OP_ORA) break;
case 0x0D: /* ab     */ ARD_AB(OP_ORA) break;
case 0x19: /* aby    */ ARD_ABY(OP_ORA) break;
case 0x12: /* zpind  */ ARD_ZPIND(OP_ORA) break;
/**** pha ****/
case 0x48: /* none   */ OP_PHA break;
/**** php ****/
case 0x08: /* none   */ OP_PHP break;
/**** phx ****/
case 0xDA: /* none   */ OP_PHX break;
/**** phy ****/
case 0x5A: /* none   */ OP_PHY break;
/**** pla ****/
case 0x68: /* none   */ OP_PLA break;
/**** plp ****/
case 0x28: /* none   */ OP_PLP goto _no_T_reset;
/**** plx ****/
case 0xFA: /* none   */ OP_PLX break;
/**** ply ****/
case 0x7A: /* none   */ OP_PLY break;
/**** rmb0 ****/
case 0x07: /* zp     */ ARW_ZP(OP_RMB0) break;
/**** rmb1 ****/
case 0x17: /* zp     */ ARW_ZP(OP_RMB1) break;
/**** rmb2 ****/
case 0x27: /* zp     */ ARW_ZP(OP_RMB2) break;
/**** rmb3 ****/
case 0x37: /* zp     */ ARW_ZP(OP_RMB3) break;
/**** rmb4 ****/
case 0x47: /* zp     */ ARW_ZP(OP_RMB4) break;
/**** rmb5 ****/
case 0x57: /* zp     */ ARW_ZP(OP_RMB5) break;
/**** rmb6 ****/
case 0x67: /* zp     */ ARW_ZP(OP_RMB6) break;
/**** rmb7 ****/
case 0x77: /* zp     */ ARW_ZP(OP_RMB7) break;
/**** rol ****/
case 0x2E: /* ab     */ ARW_AB(OP_ROL) break;
case 0x2A: /* a      */ ARW_A(OP_ROL) break;
case 0x36: /* zpx    */ ARW_ZPX(OP_ROL) break;
case 0x3E: /* abx    */ ARW_ABX(OP_ROL) break;
case 0x26: /* zp     */ ARW_ZP(OP_ROL) break;
/**** ror ****/
case 0x66: /* zp     */ ARW_ZP(OP_ROR) break;
case 0x7E: /* abx    */ ARW_ABX(OP_ROR) break;
case 0x6E: /* ab     */ ARW_AB(OP_ROR) break;
case 0x6A: /* a      */ ARW_A(OP_ROR) break;
case 0x76: /* zpx    */ ARW_ZPX(OP_ROR) break;
/**** rti ****/
case 0x40: /* none   */ OP_RTI goto _no_T_reset;
/**** rts ****/
case 0x60: /* none   */ OP_RTS break;
/**** sax ****/
case 0x22: /* none   */ OP_SAX break;
/**** say ****/
case 0x42: /* none   */ OP_SAY break;
/**** sbc ****/
case 0xFD: /* abx    */ ARD_ABX(OP_SBC) break;
case 0xE5: /* zp     */ ARD_ZP(OP_SBC) break;
case 0xE1: /* ix     */ ARD_IX(OP_SBC) break;
case 0xE9: /* im     */ ARD_IM(OP_SBC) break;
case 0xF9: /* aby    */ ARD_ABY(OP_SBC) break;
case 0xF5: /* zpx    */ ARD_ZPX(OP_SBC) break;
case 0xF2: /* zpind  */ ARD_ZPIND(OP_SBC) break;
case 0xF1: /* iy     */ ARD_IY(OP_SBC) break;
case 0xED: /* ab     */ ARD_AB(OP_SBC) break;
/**** sec ****/
case 0x38: /* none   */ OP_SEC break;
/**** sed ****/
case 0xF8: /* none   */ OP_SED break;
/**** sei ****/
case 0x78: /* none   */ OP_SEI break;
/**** set ****/
case 0xF4: /* none   */ OP_SET goto _no_T_reset;
/**** smb0 ****/
case 0x87: /* zp     */ ARW_ZP(OP_SMB0) break;
/**** smb1 ****/
case 0x97: /* zp     */ ARW_ZP(OP_SMB1) break;
/**** smb2 ****/
case 0xA7: /* zp     */ ARW_ZP(OP_SMB2) break;
/**** smb3 ****/
case 0xB7: /* zp     */ ARW_ZP(OP_SMB3) break;
/**** smb4 ****/
case 0xC7: /* zp     */ ARW_ZP(OP_SMB4) break;
/**** smb5 ****/
case 0xD7: /* zp     */ ARW_ZP(OP_SMB5) break;
/**** smb6 ****/
case 0xE7: /* zp     */ ARW_ZP(OP_SMB6) break;
/**** smb7 ****/
case 0xF7: /* zp     */ ARW_ZP(OP_SMB7) break;
/**** st0 ****/
case 0x03: /* im     */ ARD_IM(OP_ST0) break;
/**** st1 ****/
case 0x13: /* im     */ ARD_IM(OP_ST1) break;
/**** st2 ****/
case 0x23: /* im     */ ARD_IM(OP_ST2) break;
/**** sta ****/
case 0x81: /* ix     */ AWR_IX(OP_STA) break;
case 0x95: /* zpx    */ AWR_ZPX(OP_STA) break;
case 0x9D: /* abx    */ AWR_ABX(OP_STA) break;
case 0x85: /* zp     */ AWR_ZP(OP_STA) break;
case 0x99: /* aby    */ AWR_ABY(OP_STA) break;
case 0x8D: /* ab     */ AWR_AB(OP_STA) break;
case 0x92: /* zpind  */ AWR_ZPIND(OP_STA) break;
case 0x91: /* iy     */ AWR_IY(OP_STA) break;
/**** stx ****/
case 0x96: /* zpy    */ AWR_ZPY(OP_STX) break;
case 0x8E: /* ab     */ AWR_AB(OP_STX) break;
case 0x86: /* zp     */ AWR_ZP(OP_STX) break;
/**** sty ****/
case 0x84: /* zp     */ AWR_ZP(OP_STY) break;
case 0x94: /* zpx    */ AWR_ZPX(OP_STY) break;
case 0x8C: /* ab     */ AWR_AB(OP_STY) break;
/**** stz ****/
case 0x64: /* zp     */ AWR_ZP(OP_STZ) break;
case 0x74: /* zpx    */ AWR_ZPX(OP_STZ) break;
case 0x9C: /* ab     */ AWR_AB(OP_STZ) break;
case 0x9E: /* abx    */ AWR_ABX(OP_STZ) break;
/**** sxy ****/
case 0x02: /* none   */ OP_SXY break;
/**** tai ****/
case 0xF3: /* blk    */ ARD_BLK(OP_TAI) break;
/**** tam ****/
case 0x53: /* im     */ ARD_IM(OP_TAM) break;
/**** tax ****/
case 0xAA: /* none   */ OP_TAX break;
/**** tay ****/
case 0xA8: /* none   */ OP_TAY break;
/**** tdd ****/
case 0xC3: /* blk    */ ARD_BLK(OP_TDD) break;
/**** tia ****/
case 0xE3: /* blk    */ ARD_BLK(OP_TIA) break;
/**** tii ****/
case 0x73: /* blk    */ ARD_BLK(OP_TII) break;
/**** tin ****/
case 0xD3: /* blk    */ ARD_BLK(OP_TIN) break;
/**** tma ****/
case 0x43: /* im     */ ARD_IM(OP_TMA) break;
/**** trb ****/
case 0x14: /* zp     */ ARW_ZP(OP_TRB) break;
case 0x1C: /* ab     */ ARW_AB(OP_TRB) break;
/**** tsb ****/
case 0x04: /* zp     */ ARW_ZP(OP_TSB) break;
case 0x0C: /* ab     */ ARW_AB(OP_TSB) break;
/**** tst ****/
case 0x83: /* imzp   */ ARD_IMZP(OP_TST) break;
case 0xB3: /* imabx  */ ARD_IMABX(OP_TST) break;
case 0x93: /* imab   */ ARD_IMAB(OP_TST) break;
case 0xA3: /* imzpx  */ ARD_IMZPX(OP_TST) break;
/**** tsx ****/
case 0xBA: /* none   */ OP_TSX break;
/**** txa ****/
case 0x8A: /* none   */ OP_TXA break;
/**** txs ****/
case 0x9A: /* none   */ OP_TXS break;
/**** tya ****/
case 0x98: /* none   */ OP_TYA break;
