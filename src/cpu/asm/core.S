#define REG_BOUNDS_CHECKING 0
#define DO_CYCLE_COUNTING 0

#define CPUV_RESET 0xFFFE
#define CPUV_NMI 0xFFFC
#define CPUV_TIQ 0xFFFA
#define CPUV_IRQ1 0xFFF8
#define CPUV_IRQ2 0xFFF6
#define CPUV_BRK CPUV_IRQ2

#define PFLG_C 0x01
#define PFLG_Z 0x02
#define PFLG_I 0x04
#define PFLG_D 0x08
#define PFLG_B 0x10
#define PFLG_T 0x20
#define PFLG_V 0x40
#define PFLG_N 0x80

#define ZP_BASE 0x2000

	/* r29 = PC */
	/* r28 = S */
	/* r27 = P */
	/* r26 = A */
	/* r25 = X */
	/* r24 = Y */
	/* r23 = param */
	/* r22 = adr */
	/* r21 = tables */
	/* r5 = g_cpu */

#define OPFUNTBL_OFFS	(0)
#define OPCYCTBL_OFFS	(OPFUNTBL_OFFS + 256*4)
#define ZNTBL_OFFS	(OPCYCTBL_OFFS + 256)

#define ESTK_TMPADR	0
#define ESTK_TMPACC	4
#define ESTK_TSTNN	8

#define ESTK_BLKSRC	12
#define ESTK_BLKDST	16
#define ESTK_BLKLEN	20

#define ESTK_TSAVE_A	24

#define ERG_PC		r29
#define ERG_S		r28
#define ERG_P		r27
#define ERG_A		r26
#define ERG_X		r25
#define ERG_Y		r24
#define ERG_PARAM	r23
#define ERG_ADR		r22
#define ERG_TBL		r21
#define ERG_CPU		r5

	.section .text
	.align	4
	.global	_g_cpu

_g_cpu:
/*struct CpuState {*/
/*  s32 cyc_left;*/
cpu_cyc_left:	.long	0

/*  u16 pc;*/
cpu_pc:	.short	0
/*  u16 pad1; */
	.short	0
/*  u8 s;*/
cpu_s:	.byte	0
/*  u8 pad2_fix1;
	.byte	1
/*  u8 a;*/
cpu_a:	.byte	0
/*  u8 p;*/
cpu_p:	.byte	0
/*  u8 x;*/
cpu_x:	.byte	0
/*  u8 y;*/
cpu_y:	.byte	0
/*  u8 speed;*/
cpu_speed:	.byte	0
/*  u8 pad3;*/
	.byte	0
/*  u8 irq_mask;*/
cpu_irq_mask:	.byte	0
/*  u8 irq_pend;*/
cpu_irq_pend:	.byte	0
/*};*/

	.align	4


	.global	_regbounds_failed
_regbounds_failed:
	mov	lp, r1
1:
	jal	_dbgbreak
	jr	1b

	.global	_cpucore_init
	.global	_cpu_run
	.global	_cpu_run_once
	.global	_cpu_push
	.global	_cpu_pop

_cpucore_init:
	jmp	[lp]

/* s32 cpu_run(s32 cycles) */
_cpu_run:
	addi	-84, sp, sp
	st.w	lp, 44[sp]
	st.w	r29, 48[sp]
	st.w	r28, 52[sp]
	st.w	r27, 56[sp]
	st.w	r26, 60[sp]
	st.w	r25, 64[sp]
	st.w	r24, 68[sp]
	st.w	r23, 72[sp]
	st.w	r22, 76[sp]
	st.w	r21, 80[sp]

	/* load registers from state */
	movhi	hi(_g_cpu), r0, r1
	movea	lo(_g_cpu), r1, ERG_CPU
	ld.w	0x04[ERG_CPU], ERG_PC
	ld.h	0x08[ERG_CPU], ERG_S
	ld.b	0x0B[ERG_CPU], ERG_P
	ld.b	0x0A[ERG_CPU], ERG_A
	ld.b	0x0C[ERG_CPU], ERG_X
	ld.b	0x0D[ERG_CPU], ERG_Y
	movhi	hi(_cpu_tbl_base), r0, r1
	movea	lo(_cpu_tbl_base), r1, ERG_TBL

	andi	0xFF, ERG_P, ERG_P
	andi	0xFF, ERG_A, ERG_A
	andi	0xFF, ERG_X, ERG_X
	andi	0xFF, ERG_Y, ERG_Y

	st.w	r0, ESTK_TSAVE_A[sp]

	/* r6 is the cycles to add */
	ld.w	0x00[ERG_CPU], r10
	add	r6, r10
	st.w	r10, 0x00[ERG_CPU]
cpu_run_loop:
opcode_end:
	andi	~PFLG_T, ERG_P, ERG_P
opcode_end_skip_T:
#if DO_CYCLE_COUNTING
	ld.w	0x00[ERG_CPU], r10
	cmp	0, r10
/* If we're greater than 0 cycles left, do instruction */
	ble	cpu_run_done	/* use a blt because a taken branch is 2 cycles slower */
#endif

#if REG_BOUNDS_CHECKING
	movea	0xFF00, r0, r10
	movhi	0xFFFF, r0, r11
	movea	0xDE00, r0, r14
	mov	0, r13
	mov	ERG_A, r12
	and	r10, r12
	be	1f
	jal	_regbounds_failed
1:	add	1, r13
	mov	ERG_X, r12
	and	r10, r12
	be	1f
	jal	_regbounds_failed
1:	add	1, r13
	mov	ERG_Y, r12
	and	r10, r12
	be	1f
	jal	_regbounds_failed
1:	add	1, r13
	mov	ERG_P, r12
	and	r10, r12
	be	1f
	jal	_regbounds_failed
1:	add	1, r13
	mov	ERG_PC, r12
	and	r11, r12
	be	1f
	jal	_regbounds_failed
1:	add	1, r13
	mov	ERG_S, r12
	and	r14, r12
	be	1f
	jal	_regbounds_failed
1:
#endif
	/* If I flag is clear, we can do interrupts */
	andi	PFLG_I, ERG_P, r0
	bne	1f
	/* If irq_pend & ~irq_mask != 0, we have IRQs to service */
	ld.b	0x10[ERG_CPU], r10 /* g_cpu.irq_mask */
	ld.b	0x11[ERG_CPU], r11 /* g_cpu.irq_pend */
	not	r10, r10
	and	r11, r10
	be	1f

	andi	0x4, r10, r0
	be	2f
	ori	CPUV_TIQ, r0, r6
	jal	_cpu_irq_int
	jr	1f
2:
	andi	0x2, r10, r0
	be	2f
	ori	CPUV_IRQ1, r0, r6
	jal	_cpu_irq_int
	jr	1f
2:
	andi	0x1, r10, r0
	be	1f
	ori	CPUV_IRQ2, r0, r6
	jal	_cpu_irq_int
1:
/* Run a single instruction */
	mov	ERG_PC, r6
	jal	_mem_fetch8
	andi	0xFF, r10, r10
	jal	_dbgbreak
	add	1, ERG_PC
	andi	0xFFFF, ERG_PC, ERG_PC
/* Calculate cycles for the instruction */
#if DO_CYCLE_COUNTING
	mov	r10, r11
	shl	2, r10
	add	ERG_TBL, r11
	add	ERG_TBL, r10
	ld.w	0x00[ERG_CPU], r12
	ld.b	OPCYCTBL_OFFS[r11], r11
	add	r11, r12
	st.w	r12, 0x00[ERG_CPU]
#else
	shl	2, r10
	add	ERG_TBL, r10
#endif
/* opcode table is a list of jr opcodes, so just jump into the address */
	jmp	[r10]

cpu_run_done:
	/* store registers to state */
	st.h	ERG_PC, 0x04[ERG_CPU]
	st.b	ERG_S, 0x08[ERG_CPU]
	st.b	ERG_P, 0x0B[ERG_CPU]
	st.b	ERG_A, 0x0A[ERG_CPU]
	st.b	ERG_X, 0x0C[ERG_CPU]
	st.b	ERG_Y, 0x0D[ERG_CPU]

	/* We're done, leave. */
	ld.w	80[sp], r21
	ld.w	76[sp], r22
	ld.w	72[sp], r23
	ld.w	68[sp], r24
	ld.w	64[sp], r25
	ld.w	60[sp], r26
	ld.w	56[sp], r27
	ld.w	52[sp], r28
	ld.w	48[sp], r29
	ld.w	44[sp], lp
	addi	84, sp, sp
	jmp	[lp]

_cpu_run_once:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	movhi	hi(_g_cpu), r0, r1
	ld.w	lo(_g_cpu)[r1], r6
	not	r6, r6
	add	2, r6
	jal	_cpu_run
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_push_int:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	mov	r6, r7
	mov	ERG_S, r6
	jal	_mem_w8
	add	-1, ERG_S
	st.b	ERG_S, 0x08[ERG_CPU]
	ld.h	0x08[ERG_CPU], ERG_S
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_pop_int:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	add	1, ERG_S
	st.b	ERG_S, 0x08[ERG_CPU]
	ld.h	0x08[ERG_CPU], ERG_S
	mov	ERG_S, r6
	jal	_mem_r8
	andi	0xFF, r10, r10
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_irq_int:
	addi	-8, sp, sp
	st.w	lp, 0[sp]
	st.h	r6, 4[sp]
	st.h	r0, 6[sp]
	mov	ERG_PC, r6
	shr	8, r6
	jal	_cpu_push_int
	andi	0xFF, ERG_PC, r6
	jal	_cpu_push_int
	andi	~PFLG_B, ERG_P, r6
	jal	_cpu_push_int
	ld.w	4[sp], r6
	addi	1, r6, r7
	st.h	r7, 4[sp]
	jal	_mem_r8
	andi	0xFF, r10, ERG_PC
	ld.w	4[sp], r6
	jal	_mem_r8
	shl	8, r10
	or	r10, ERG_PC
	andi	~(PFLG_D|PFLG_T), ERG_P, ERG_P
	ori	PFLG_I, ERG_P, ERG_P
	andi	0xFFFF, ERG_PC, ERG_PC
	ld.w	0[sp], lp
	addi	8, sp, sp
	jmp	[lp]

_cpu_makecall_int:
	addi	-8, sp, sp
	st.w	lp, 0[sp]
	st.h	r6, 4[sp]
	st.h	r0, 6[sp]
	add	-1, ERG_PC
	mov	ERG_PC, r6
	shr	8, r6
	jal	_cpu_push_int
	andi	0xFF, ERG_PC, r6
	jal	_cpu_push_int
	ld.w	4[sp], ERG_PC
	ld.w	0[sp], lp
	addi	8, sp, sp
	jmp	[lp]

_cpu_push:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	mov	r6, r7
	movhi	hi(_g_cpu+8), r0, r1
	ld.h	lo(_g_cpu+8)[r1], r6
	jal	_mem_w8
	movhi	hi(_g_cpu+8), r0, r1
	ld.b	lo(_g_cpu+8)[r1], r6
	add	-1, r6
	st.b	r6, lo(_g_cpu+8)[r1]
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_pop:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	movhi	hi(_g_cpu+8), r0, r1
	ld.h	lo(_g_cpu+8)[r1], r6
	add	1, r6
	st.b	r6, lo(_g_cpu+8)[r1]
	ld.h	lo(_g_cpu+8)[r1], r6
	jal	_mem_r8
	andi	0xFF, r10, r10
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]
	
/* opcode functions should end with a jr to opcode_end */
cpuop_BAD:
	jr	opcode_end

#include "prims.inc.S"
#include "ops.inc.S"
