#ifndef CPU_JIT_EMITTER_H_
#define CPU_JIT_EMITTER_H_

#define EMIT_WORD(v) do { \
  buf[insns++] = v; \
} while(0)

// Emit primitives
#define EMIT_FMT_I(op, reg1, reg2) do { \
  EMIT_WORD(((op & 0x3F) << 10) | ((reg2 & 0x1F) << 5) | ((reg1 & 0x1F) << 0)); \
} while(0)
#define EMIT_FMT_II(op, imm, reg) EMIT_FMT_I(op, imm, reg)
#define EMIT_FMT_III(op, cond, disp) do { \
  EMIT_WORD(((op & 0x7) << 13) | ((cond & 0xF) << 9) | ((disp & 0x1FF) << 0)); \
} while(0)
#define EMIT_FMT_IV(op, disp) do { \
  EMIT_WORD(((op & 0x3F) << 10) | (((disp >> 16) & 0x3FF) << 0)); \
  EMIT_WORD(disp & 0xFFFF); \
} while(0)
#define EMIT_FMT_V(op, reg1, reg2, imm) do { \
  EMIT_FMT_I(op, reg1, reg2); \
  EMIT_WORD(imm); \
} while(0)
#define EMIT_FMT_VI(op, reg1, reg2, disp) do { \
  EMIT_FMT_VI(op, reg1, reg2, disp); \
} while(0)
#define EMIT_FMT_VII(op, reg1, reg2, sub) do { \
  EMIT_FMT_I(op, reg1, reg2); \
  EMIT_WORD((sub & 0x3F) << 10); \
} while(0)

// Instruction emitters, add as necessary
#define EMIT_ADD_R_R(reg1, reg2) EMIT_FMT_I(0x01, reg1, reg2)
#define EMIT_ADDI_I_R_R(imm, reg1, reg2) EMIT_FMT_V(0x29, reg1, reg2, imm)

#define EMIT_ANDI_I_R_R(imm, reg1, reg2) EMIT_FMT_V(0x2D, reg1, reg2, imm)

#define EMIT_CMP_R_R(reg1, reg2) EMIT_FMT_I(0x03, reg1, reg2)

#define EMIT_SHL_I_R(imm, reg) EMIT_FMT_II(0x14, imm, reg)

#define EMIT_LDB_DISP_R_R(disp, src, reg) EMIT_FMT_VI(0x30, src, reg, disp)
#define EMIT_LDH_DISP_R_R(disp, src, reg) EMIT_FMT_VI(0x31, src, reg, disp)
#define EMIT_LDW_DISP_R_R(disp, src, reg) EMIT_FMT_VI(0x33, src, reg, disp)

#define EMIT_LDB_RAMI_R(addr, reg) EMIT_LDB_DISP_R_R(addr, JREG_RAM, reg)
#define EMIT_LDH_RAMI_R(addr, reg) EMIT_LDH_DISP_R_R(addr, JREG_RAM, reg)
#define EMIT_LDW_RAMI_R(addr, reg) EMIT_LDW_DISP_R_R(addr, JREG_RAM, reg)
#define EMIT_LDB_RAMR_R(addr, reg) do { \
  EMIT_ADD_R_R(JREG_RAM, addr); \
  EMIT_LDB_DISP_R_R(0, addr, reg); \
} while(0)
#define EMIT_LDH_RAMR_R(addr, reg) do { \
  EMIT_ADD_R_R(JREG_RAM, addr); \
  EMIT_LDH_DISP_R_R(0, addr, reg); \
} while(0)

// TODO: These aren't even close to right (they need to deal with MPR)
#define EMIT_LDB_MEMI_R(addr, reg) EMIT_LDB_DISP_R_R(addr, JREG_RAM, reg)
#define EMIT_LDH_MEMI_R(addr, reg) EMIT_LDH_DISP_R_R(addr, JREG_RAM, reg)
#define EMIT_LDB_MEMR_R(addr, reg) EMIT_LDB_DISP_R_R(0, addr, reg)
#define EMIT_LDH_MEMR_R(addr, reg) EMIT_LDH_DISP_R_R(0, addr, reg)

#endif
