#include "top.h"

#include "cpu/cpu.h"
#include "cpu/jitc/compiler.h"

/* JIT register allocation:
 *
 * A: r20
 * X: r21
 * Y: r22
 * S: r23
 * P: r24
 * CY: r25
 * RAM: r26
 *
 * Temporaries:
 * r12~r19
 */

#define JFLAG_C 0x01
#define JFLAG_Z 0x02
#define JFLAG_I 0x04
#define JFLAG_D 0x08
#define JFLAG_V 0x40
#define JFLAG_N 0x80

#define JREG_TMP0 12
#define JREG_TMP1 13
#define JREG_TMP2 14

#define JREG_A 20
#define JREG_X 21
#define JREG_Y 22
#define JREG_S 23
#define JREG_P 24
#define JREG_CY 25
#define JREG_RAM 26

#define MAX_EMIT_SIZE 8

#define EMITM_READ_ZP(tmp, dst) do { \
  tmp = g_cpurom[pc++]; \
  EMIT_LDB_RAMI_R(tmp, dst); \
} while(0)

// ea is an offset from 6502.MEM
#define EMITM_READ_IND(tmp, dst, ea) do { \
  tmp = g_cpurom[pc++]; \
  EMIT_LDB_RAMI_R(tmp, ea); \
  EMIT_LDB_MEMR_R(ea, dst); \
} while(0)
  
#define EMITM_READ_ABS(tmp, dst) do { \
  tmp = g_cpurom[pc++]; \
  tmp |= g_cpurom[pc++] << 8; \
  EMIT_LDB_MEMI_R(tmp, dst); \
} while(0)

// ea is an offset from 6502.ZP
#define EMITM_READ_ZP_IDX(tmp, dst, reg, ea) do { \
  tmp = g_cpurom[pc++]; \
  EMIT_ADDI_I_R_R(tmp, reg, dst); \
  EMIT_ANDI_I_R_R(0xFF, dst, ea); \
  EMIT_LDB_RAMR_R(ea, dst); \
} while(0)
#define EMITM_READ_ZP_X(tmp, dst, ea) EMITM_READ_ZP_IDX(tmp, dst, JREG_X, ea)
#define EMITM_READ_ZP_Y(tmp, dst, ea) EMITM_READ_ZP_IDX(tmp, dst, JREG_Y, ea)
  
// ea is an offset from 6502.MEM
#define EMITM_READ_ABS_IDX(tmp, dst, reg, ea) do { \
  tmp = g_cpurom[pc++]; \
  tmp |= g_cpurom[pc++] << 8; \
  EMIT_ADDI_I_R_R(tmp, reg, dst); \
  EMIT_ANDI_I_R_R(0xFFFF, dst, ea); \
  EMIT_LDB_MEMR_R(ea, dst); \
} while(0)
#define EMITM_READ_ABS_X(tmp, dst, ea) EMITM_READ_ABS_IDX(tmp, dst, JREG_X, ea)
#define EMITM_READ_ABS_Y(tmp, dst, ea) EMITM_READ_ABS_IDX(tmp, dst, JREG_Y, ea)

// NOTE: Does _NOT_ handle zero-page wrapping behavior on the two bytes
// ea is an offset from 6502.ZP
#define EMITM_READ_IDX_IND(tmp, dst, ea) do { \
  tmp = g_cpurom[pc++]; \
  EMIT_ADDI_I_R_R(tmp, JREG_X, dst); \
  EMIT_ANDI_I_R_R(0xFF, dst, ea); \
  EMIT_LDH_RAMR_R(ea, dst); \
} while(0)

// NOTE: Does _NOT_ handle zero-page wrapping behavior on the two bytes
// ea is an offset from 6502.MEM
#define EMITM_READ_IND_IDX(tmp, dst, ea) do { \
  tmp = g_cpurom[pc++]; \
  EMIT_LDH_RAMI_R(tmp, dst); \
  EMIT_ADD_R_R(JREG_Y, dst); \
  EMIT_ANDI_I_R_R(0xFFFF, dst, ea); \
  EMIT_LDH_RAMR_R(ea, dst); \
} while(0)

#define EMITM_FLAG_NZ(reg) do { \
  /* TODO: flags lol */; \
} while(0)

#define EMITM_FLAG_C_FROM_N(reg) do { \
  /* TODO: C = N from REG */; \
} while(0)

#define EMITM_FLAG_NZVC_ADD(reg) do { \
  /* TODO: flags lol */; \
} while(0)

#define EMITT_BEQ_DISP(disp) do { \
  /* TODO: lol */; \
} while(0)

#define EMITT_BNE_DISP(disp) do { \
  /* TODO: lol */; \
} while(0)

#define EMITT_BRA_DISP(disp) do { \
  /* TODO: lol */; \
} while(0)

#define EMITT_REQUIRE_CY() do { \
  /* If necessary, fast-forward CY to now */ \
} while(0)
#define EMITT_REQUIRE_Z() do { \
  /* If necessary, fast-forward Z to now */ \
} while(0)
#define EMITT_REQUIRE_N() do { \
  /* If necessary, fast-forward N to now */ \
} while(0)
#define EMITT_REQUIRE_V() do { \
  /* If necessary, fast-forward V to now */ \
} while(0)

#define EMITT_READ_ZP() EMITM_READ_ZP(tmp8[0], JREG_TMP0)
#define EMITT_READ_ZP_X() EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP0)
#define EMITT_READ_IND() EMITM_READ_IND(tmp8[0], JREG_TMP0, JREG_TMP0)
#define EMITT_READ_IDX_IND() EMITM_READ_IDX_IND(tmp8[0], JREG_TMP0, JREG_TMP0)
#define EMITT_READ_IND_IDX() EMITM_READ_IND_IDX(tmp8[0], JREG_TMP0, JREG_TMP0)
#define EMITT_READ_ABS() EMITM_READ_ABS(tmp16[0], JREG_TMP0)
#define EMITT_READ_ABS_X() EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP0)
#define EMITT_READ_ABS_Y() EMITM_READ_ABS_Y(tmp16[0], JREG_TMP0, JREG_TMP0)

u32 jit_compiler(u32 pc, u16 *buf, u32 max_insns) {
  u32 insns = 0;
  u32 opc = pc;
  while(insns < max_insns - MAX_EMIT_SIZE) {
    u8 i = g_cpurom[pc++];
    u8 tmp8[8];
    u16 tmp16[2];
// http://shu.emuunlim.com/download/pcedocs/pce_cpu.html
    switch(i) {
///////////////////////////////////////////////////////////////////////////////
// ADC
///////////////////////////////////////////////////////////////////////////////
#define EMITT_ADC() do { \
  EMIT_ANDI_I_R_R(0xFF, JREG_A, JREG_A); \
  EMIT_ADD_R_R(JREG_TMP0, JREG_A); \
  EMIT_ADD_R_R(JREG_CY, JREG_A); \
  EMITM_FLAG_NVZC_ADD(JREG_A); \
} while(0)
      case 0x69: // ADC #nn
        tmp8[0] = g_cpurom[pc++];
        EMIT_ANDI_I_R_R(0xFF, JREG_A, JREG_A);
        EMIT_ADDI_I_R_R(tmp8[0], JREG_A, JREG_A);
        EMIT_ADD_R_R(JREG_CY, JREG_A);
        EMITM_FLAG_NVZC_ADD(JREG_A);
        break;
      case 0x65: // ADC zz
        EMITT_READ_ZP();      EMITT_ADC();  break;
      case 0x75: // ADC zz, X
        EMITM_READ_ZP_X();    EMITT_ADC();  break;
      case 0x72: // ADC (zz)
        EMITM_READ_IND();     EMITT_ADC();  break;
      case 0x61: // ADC (zz, X)
        EMITM_READ_IDX_IND(); EMITT_ADC();  break;
      case 0x71: // ADC (zz), Y
        EMITM_READ_IND_IDX(); EMITT_ADC();  break;
      case 0x6D: // ADC hhll
        EMITM_READ_ABS();     EMITT_ADC();  break;
      case 0x7D: // ADC hhll, X
        EMITM_READ_ABS_X();   EMITT_ADC();  break;
      case 0x79: // ADC hhll, Y
        EMITM_READ_ABS_Y();   EMITT_ADC();  break;
///////////////////////////////////////////////////////////////////////////////
// AND
///////////////////////////////////////////////////////////////////////////////
#define EMITT_AND() do { \
  EMIT_AND_R_R(JREG_TMP0, JREG_A); \
  EMITM_FLAG_NZ(JREG_A); \
} while(0)
      case 0x29: // AND #nn
        tmp8[0] = g_cpurom[pc++];
        EMIT_ANDI_I_R_R(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0x25: // AND zz
        EMITT_READ_ZP();      EMITT_AND();  break;
      case 0x35: // AND zz, X
        EMITT_READ_ZP_X();    EMITT_AND();  break;
      case 0x32: // AND (zz)
        EMITT_READ_IND();     EMITT_AND();  break;
      case 0x21: // AND (zz, X)
        EMITT_READ_IDX_IND(); EMITT_AND();  break;
      case 0x31: // AND (zz), Y
        EMITT_READ_IND_IDX(); EMITT_AND();  break;
      case 0x2D: // AND hhll
        EMITT_READ_ABS();     EMITT_AND();  break;
      case 0x3D: // AND hhll, X
        EMITM_READ_ABS_X();   EMITT_AND();  break;
      case 0x39: // AND hhll, Y
        EMITM_READ_ABS_Y();   EMITT_AND();  break;
///////////////////////////////////////////////////////////////////////////////
// ASL
///////////////////////////////////////////////////////////////////////////////
#define EMITT_ASL(reg) do { \
  EMITM_FLAG_C_FROM_N(reg); \
  EMIT_SHL_I_R(1, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0x06: // ASL zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_ASL(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0x16: // ASL zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_ASL(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x0E: // ASL hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_ASL(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0x1E: // ASL hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_ASL(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x0A: // ASL A
        EMITT_ASL(JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// BBRi/BBSi
///////////////////////////////////////////////////////////////////////////////
#define EMITT_BBx(bit, func) do { \
  EMITM_READ_ZP(tmp8[0], JREG_TMP0); \
  tmp8[1] = g_cpurom[pc++]; \
  EMIT_ANDI((1 << bit), JREG_TMP0, 0); \
  func(tmp8[1]); \
} while(0)
#define EMITT_BBR(bit) EMITT_BBx(bit, EMITT_BEQ_DISP)
#define EMITT_BBS(bit) EMITT_BBx(bit, EMITT_BNE_DISP)
      case 0x0F: // BBR0 ZZ, hhll
        EMITT_BBR(0); break;
      case 0x1F: // BBR1 ZZ, hhll
        EMITT_BBR(1); break;
      case 0x2F: // BBR2 ZZ, hhll
        EMITT_BBR(2); break;
      case 0x3F: // BBR3 ZZ, hhll
        EMITT_BBR(3); break;
      case 0x4F: // BBR4 ZZ, hhll
        EMITT_BBR(4); break;
      case 0x5F: // BBR5 ZZ, hhll
        EMITT_BBR(5); break;
      case 0x6F: // BBR6 ZZ, hhll
        EMITT_BBR(6); break;
      case 0x7F: // BBR7 ZZ, hhll
        EMITT_BBR(7); break;
      case 0x8F: // BBS0 ZZ, hhll
        EMITT_BBS(0); break;
      case 0x9F: // BBS1 ZZ, hhll
        EMITT_BBS(1); break;
      case 0xAF: // BBS2 ZZ, hhll
        EMITT_BBS(2); break;
      case 0xBF: // BBS3 ZZ, hhll
        EMITT_BBS(3); break;
      case 0xCF: // BBS4 ZZ, hhll
        EMITT_BBS(4); break;
      case 0xDF: // BBS5 ZZ, hhll
        EMITT_BBS(5); break;
      case 0xEF: // BBS6 ZZ, hhll
        EMITT_BBS(6); break;
      case 0xFF: // BBS7 ZZ, hhll
        EMITT_BBS(7); break;
///////////////////////////////////////////////////////////////////////////////
// Bcc
///////////////////////////////////////////////////////////////////////////////
      case 0x90: // BCC hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_CY();
        EMIT_CMP_R_R(0, JREG_CY);
        EMITT_BEQ_DISP(tmp8[0]); break;
      case 0xB0: // BCS hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_CY();
        EMIT_CMP_R_R(0, JREG_CY);
        EMITT_BNE_DISP(tmp8[0]); break;
      case 0xF0: // BEQ hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_Z();
        EMIT_ANDI_I_R_R(JFLAG_Z, JREG_P, 0);
        EMITT_BNE_DISP(tmp8[0]); break;
      case 0xD0: // BNE hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_Z();
        EMIT_ANDI_I_R_R(JFLAG_Z, JREG_P, 0);
        EMITT_BEQ_DISP(tmp8[0]); break;
      case 0x30: // BMI hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_N();
        EMIT_ANDI_I_R_R(JFLAG_N, JREG_P, 0);
        EMITT_BNE_DISP(tmp8[0]); break;
      case 0x10: // BPL hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_N();
        EMIT_ANDI_I_R_R(JFLAG_N, JREG_P, 0);
        EMITT_BEQ_DISP(tmp8[0]); break;
      case 0x70: // BVS hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_V();
        EMIT_ANDI_I_R_R(JFLAG_V, JREG_P, 0);
        EMITT_BNE_DISP(tmp8[0]); break;
      case 0x50: // BVC hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_REQUIRE_V();
        EMIT_ANDI_I_R_R(JFLAG_V, JREG_P, 0);
        EMITT_BEQ_DISP(tmp8[0]); break;
///////////////////////////////////////////////////////////////////////////////
// BRA
///////////////////////////////////////////////////////////////////////////////
      case 0x80: // BRA hhll
        tmp8[0] = g_cpurom[pc++];
        EMITT_BRA_DISP(tmp8[0]); break;
///////////////////////////////////////////////////////////////////////////////
// BIT
///////////////////////////////////////////////////////////////////////////////
#define EMITT_BIT() do { \
  /* TODO: Fucking fuck off fucking shit flags (N/V) */ \
  EMIT_AND_R_R(JREG_A, JREG_TMP0); \
  EMITM_FLAG_Z(JREG_TMP0); \
} while(0)
      case 0x89: // BIT #nn
        tmp8[0] = g_cpurom[pc++];
        if((~tmp8[0]) & 0xC0) {
          EMIT_ANDI_I_R_R(~0xC0, JREG_P, JREG_P);
        }
        if(tmp8[0] & 0xC0) {
          EMIT_ORI_I_R_R(tmp8[0] & 0xC0, JREG_P, JREG_P);
        }
        EMIT_ANDI_I_R_R(tmp8[0], JREG_A, JREG_TMP0);
        EMITM_FLAG_Z(JREG_TMP0);
        break;
      case 0x24: // BIT zz
        EMITT_READ_ZP();    EMITT_BIT();  break;
      case 0x34: // BIT zz, X
        EMITT_READ_ZP_X();  EMITT_BIT();  break;
      case 0x2C: // BIT hhll
        EMITT_READ_ABS();   EMITT_BIT();  break;
      case 0x3C: // BIT hhll, X
        EMITT_READ_ABS_X(); EMITT_BIT();  break;
///////////////////////////////////////////////////////////////////////////////
// BRK
///////////////////////////////////////////////////////////////////////////////
      case 0x00: // BRK
        // TODO: emit cpu_irq(CPUV_BRK);
        break;
///////////////////////////////////////////////////////////////////////////////
// BSR
///////////////////////////////////////////////////////////////////////////////
      case 0x44: // BSR hhll
        // TODO
        break;
///////////////////////////////////////////////////////////////////////////////
// CLC
///////////////////////////////////////////////////////////////////////////////
      case 0x18: // CLC
        EMIT_MOV_I_R(0, JREG_CY);
        break;
///////////////////////////////////////////////////////////////////////////////
// CLA
///////////////////////////////////////////////////////////////////////////////
      case 0x62: // CLA
        EMIT_MOV_I_R(0, JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// CLD
///////////////////////////////////////////////////////////////////////////////
      case 0xD8: // CLD
        // TODO: Fuck you.
        EMIT_ANDI_I_R_R(~JFLAG_D, JREG_P, JREG_P);
        break;
///////////////////////////////////////////////////////////////////////////////
// CLI
///////////////////////////////////////////////////////////////////////////////
      case 0x58: // CLI
        // TODO: Just write the value to g_cpu.
        EMIT_ANDI_I_R_R(~JFLAG_I, JREG_P, JREG_P);
        break;
    }
///////////////////////////////////////////////////////////////////////////////
// CLV
///////////////////////////////////////////////////////////////////////////////
      case 0xB8: // CLV
        EMIT_ANDI_I_R_R(~JFLAG_V, JREG_P, JREG_P);
        break;
///////////////////////////////////////////////////////////////////////////////
// CLX
///////////////////////////////////////////////////////////////////////////////
      case 0x82: // CLX
        EMIT_MOV_I_R(0, JREG_X);
        break;
///////////////////////////////////////////////////////////////////////////////
// CLY
///////////////////////////////////////////////////////////////////////////////
      case 0xC2: // CLY
        EMIT_MOV_I_R(0, JREG_Y);
        break;
///////////////////////////////////////////////////////////////////////////////
// CMP/CPX/CPY
///////////////////////////////////////////////////////////////////////////////
#define EMITT_CMP_I(reg) EMITT_SBC_I_IMPL(reg, 0, 0)
#define EMITT_CMP(reg) EMITT_SBC_IMPL(reg, 0, 0)
      case 0xC9: // CMP #nn
        tmp8[0] = g_cpurom[pc++]; EMITT_CMP_I(JREG_A); break;
      case 0xC5: // CMP zz
        EMITT_READ_ZP();          EMITT_CMP(JREG_A);  break;
      case 0xD5: // CMP zz, X
        EMITT_READ_ZP_X();        EMITT_CMP(JREG_A);  break;
      case 0xD2: // CMP (zz)
        EMITT_READ_IND();         EMITT_CMP(JREG_A);  break;
      case 0xC1: // CMP (zz, X)
        EMITT_READ_IDX_IND();     EMITT_CMP(JREG_A);  break;
      case 0xD1: // CMP (zz), Y
        EMITT_READ_IND_IDX();     EMITT_CMP(JREG_A);  break;
      case 0xCD: // CMP hhll
        EMITT_READ_ABS();         EMITT_CMP(JREG_A);  break;
      case 0xDD: // CMP hhll, X
        EMITT_READ_ABS_X();       EMITT_CMP(JREG_A);  break;
      case 0xD9: // CMP hhll, Y
        EMITT_READ_ABS_Y();       EMITT_CMP(JREG_A);  break;
      case 0xE0: // CPX #nn
        tmp8[0] = g_cpurom[pc++]; EMITT_CMP_I(JREG_X); break;
      case 0xE4: // CPX zz
        EMITT_READ_ZP();          EMITT_CMP(JREG_X);  break;
      case 0xEC: // CPX hhll
        EMITT_READ_ABS();         EMITT_CMP(JREG_X);  break;
      case 0xC0: // CPY #nn
        tmp8[0] = g_cpurom[pc++]; EMITT_CMP_I(JREG_Y); break;
      case 0xC4: // CPY zz
        EMITT_READ_ZP();          EMITT_CMP(JREG_Y);  break;
      case 0xCC: // CPY hhll
        EMITT_READ_ABS();         EMITT_CMP(JREG_Y);  break;
///////////////////////////////////////////////////////////////////////////////
// CSH/CSL
///////////////////////////////////////////////////////////////////////////////
      case 0xD4: // CSH
      case 0x54: // CSL
        // Nothing to do...
        break;
///////////////////////////////////////////////////////////////////////////////
// DEC/DEX/DEY
///////////////////////////////////////////////////////////////////////////////
#define EMITT_DEC(reg) do { \
  EMIT_ADDI_I_R_R(1, reg, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0xC6: // DEC zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_DEC(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0xD6: // DEC zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_DEC(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0xCE: // DEC hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_DEC(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0xDE: // DEC hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_DEC(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x3A: // DEC A
        EMITT_DEC(JREG_A);
        break;
      case 0xCA: // DEX
        EMITT_DEC(JREG_X);
        break;
      case 0x88: // DEY
        EMITT_DEC(JREG_Y);
        break;
///////////////////////////////////////////////////////////////////////////////
// EOR
///////////////////////////////////////////////////////////////////////////////
#define EMITT_EOR() do { \
  EMIT_XOR_R_R(JREG_TMP0, JREG_A); \
  EMITM_FLAG_NZ(JREG_A); \
} while(0)
      case 0x49: // EOR #nn
        tmp8[0] = g_cpurom[pc++];
        EMIT_XORI_I_R_R(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0x45: // EOR zz
        EMITT_READ_ZP();      EMITT_EOR();  break;
      case 0x55: // EOR zz, X
        EMITT_READ_ZP_X();    EMITT_EOR();  break;
      case 0x52: // EOR (zz)
        EMITT_READ_IND();     EMITT_EOR();  break;
      case 0x41: // EOR (zz, X)
        EMITT_READ_IDX_IND(); EMITT_EOR();  break;
      case 0x51: // EOR (zz), Y
        EMITT_READ_IND_IDX(); EMITT_EOR();  break;
      case 0x4D: // EOR hhll
        EMITT_READ_ABS();     EMITT_EOR();  break;
      case 0x5D: // EOR hhll, X
        EMITM_READ_ABS_X();   EMITT_EOR();  break;
      case 0x59: // EOR hhll, Y
        EMITM_READ_ABS_Y();   EMITT_EOR();  break;
///////////////////////////////////////////////////////////////////////////////
// INC/INX/INY
///////////////////////////////////////////////////////////////////////////////
#define EMITT_INC(reg) do { \
  EMIT_ADDI_I_R_R(1, reg, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0xE6: // INC zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_INC(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0xF6: // INC zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_INC(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0xEE: // INC hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_INC(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0xFE: // INC hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_INC(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x1A: // INC A
        EMITT_INC(JREG_A);
        break;
      case 0xE8: // INX
        EMITT_INC(JREG_X);
        break;
      case 0xC8: // INY
        EMITT_INC(JREG_Y);
        break;
///////////////////////////////////////////////////////////////////////////////
// JMP
///////////////////////////////////////////////////////////////////////////////
      case 0x4C: // JMP hhll
        // TODO: Fuck you.
        break;
      case 0x6C: // JMP (hhll)
        // TODO
        break;
      case 0x7C: // JMP hhll, X
        // TODO
        break;
///////////////////////////////////////////////////////////////////////////////
// JSR
///////////////////////////////////////////////////////////////////////////////
      case 0x20: // JSR hhll
        // TODO: Fuck you.
        break;
///////////////////////////////////////////////////////////////////////////////
// LDA
///////////////////////////////////////////////////////////////////////////////
      case 0xA9: // LDA #nn
        tmp8[0] = g_cpurom[pc++];
        EMITM_MOVEA_I_R_R(tmp8[0], 0, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xA5: // LDA zz
        EMITM_READ_ZP(tmp8[0], JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xB5: // LDA zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xB2: // LDA (zz)
        EMITM_READ_IND(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xA1: // LDA (zz, X)
        EMITM_READ_IDX_IND(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xB1: // LDA (zz), Y
        EMITM_READ_IND_IDX(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xAD: // LDA hhll
        EMITM_READ_ABS(tmp8[0], JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xBD: // LDA hhll, X
        EMITM_READ_ABS_X(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0xB9: // LDA hhll, Y
        EMITM_READ_ABS_Y(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// LDX
///////////////////////////////////////////////////////////////////////////////
      case 0xA2: // LDX #nn
        tmp8[0] = g_cpurom[pc++];
        EMITM_MOVEA_I_R_R(tmp8[0], 0, JREG_X);
        EMITM_FLAG_NZ(JREG_X);
        break;
      case 0xA6: // LDX zz
        EMITM_READ_ZP(tmp8[0], JREG_X);
        EMITM_FLAG_NZ(JREG_X);
        break;
      case 0xB6: // LDX zz, Y
        EMITM_READ_ZP_Y(tmp8[0], JREG_X, JREG_X);
        EMITM_FLAG_NZ(JREG_X);
        break;
      case 0xAE: // LDX hhll
        EMITM_READ_ABS(tmp8[0], JREG_X);
        EMITM_FLAG_NZ(JREG_X);
        break;
      case 0xBE: // LDX hhll, Y
        EMITM_READ_ABS_Y(tmp8[0], JREG_X, JREG_X);
        EMITM_FLAG_NZ(JREG_X);
        break;
///////////////////////////////////////////////////////////////////////////////
// LDY
///////////////////////////////////////////////////////////////////////////////
      case 0xA0: // LDY #nn
        tmp8[0] = g_cpurom[pc++];
        EMITM_MOVEA_I_R_R(tmp8[0], 0, JREG_Y);
        EMITM_FLAG_NZ(JREG_Y);
        break;
      case 0xA4: // LDY zz
        EMITM_READ_ZP(tmp8[0], JREG_Y);
        EMITM_FLAG_NZ(JREG_Y);
        break;
      case 0xB4: // LDY zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_Y, JREG_Y);
        EMITM_FLAG_NZ(JREG_Y);
        break;
      case 0xAC: // LDY hhll
        EMITM_READ_ABS(tmp8[0], JREG_Y);
        EMITM_FLAG_NZ(JREG_Y);
        break;
      case 0xBC: // LDY hhll, X
        EMITM_READ_ABS_X(tmp8[0], JREG_Y, JREG_Y);
        EMITM_FLAG_NZ(JREG_Y);
        break;
///////////////////////////////////////////////////////////////////////////////
// LSR
///////////////////////////////////////////////////////////////////////////////
#define EMITT_LSR(reg) do { \
  EMITM_FLAG_C_FROM_B0(reg); \
  EMIT_SHR_I_R(1, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0x46: // LSR zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_LSR(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0x56: // LSR zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_LSR(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x4E: // LSR hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_LSR(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0x5E: // LSR hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_LSR(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x4A: // LSR A
        EMITT_LSR(JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// NOP
///////////////////////////////////////////////////////////////////////////////
      case 0xEA: // NOP
        // Nothing to do...
        break;
///////////////////////////////////////////////////////////////////////////////
// ORA
///////////////////////////////////////////////////////////////////////////////
#define EMITT_ORA() do { \
  EMIT_OR_R_R(JREG_TMP0, JREG_A); \
  EMITM_FLAG_NZ(JREG_A); \
} while(0)
      case 0x09: // ORA #nn
        tmp8[0] = g_cpurom[pc++];
        EMIT_ORI_I_R_R(tmp8[0], JREG_A, JREG_A);
        EMITM_FLAG_NZ(JREG_A);
        break;
      case 0x05: // ORA zz
        EMITT_READ_ZP();      EMITT_ORA();  break;
      case 0x15: // ORA zz, X
        EMITT_READ_ZP_X();    EMITT_ORA();  break;
      case 0x12: // ORA (zz)
        EMITT_READ_IND();     EMITT_ORA();  break;
      case 0x01: // ORA (zz, X)
        EMITT_READ_IDX_IND(); EMITT_ORA();  break;
      case 0x11: // ORA (zz), Y
        EMITT_READ_IND_IDX(); EMITT_ORA();  break;
      case 0x0D: // ORA hhll
        EMITT_READ_ABS();     EMITT_ORA();  break;
      case 0x1D: // ORA hhll, X
        EMITM_READ_ABS_X();   EMITT_ORA();  break;
      case 0x19: // ORA hhll, Y
        EMITM_READ_ABS_Y();   EMITT_ORA();  break;
///////////////////////////////////////////////////////////////////////////////
// PHA/PHP/PHX/PHY
///////////////////////////////////////////////////////////////////////////////
#define EMITM_PUSH(reg) do { \
  /* TODO: lol */\
} while(0)
      case 0x48: // PHA
        EMITM_PUSH(JREG_A);   break;
      case 0x08: // PHP
        // TODO: Canonicalize flags
        EMITM_PUSH(JREG_P);break;
      case 0xDA: // PHX
        EMITM_PUSH(JREG_X);   break;
      case 0x5A: // PHY
        EMITM_PUSH(JREG_Y);   break;
///////////////////////////////////////////////////////////////////////////////
// PHA/PHP/PHX/PHY
///////////////////////////////////////////////////////////////////////////////
#define EMITM_PULL(reg) do { \
  /* TODO: lol */\
} while(0)
      case 0x68: // PLA
        EMITM_PULL(JREG_A);   break;
      case 0x28: // PLP
        EMITM_PULL(JREG_P);
        // TODO: de-canonicalize flags
        break;
      case 0xFA: // PLX
        EMITM_PULL(JREG_X);   break;
      case 0x7A: // PLY
        EMITM_PULL(JREG_Y);   break;
///////////////////////////////////////////////////////////////////////////////
// RMBi
///////////////////////////////////////////////////////////////////////////////
#define EMITT_RMB(bit) do { \
  EMITM_READ_ZP(tmp8[0], JREG_TMP0); \
  EMITM_ANDI_I_R_R(~(1 << bit), JREG_TMP0, JREG_TMP0); \
  EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]); \
} while(0)
      case 0x07: // RMB0 ZZ
        EMITT_RMB(0); break;
      case 0x17: // RMB1 ZZ
        EMITT_RMB(1); break;
      case 0x27: // RMB2 ZZ
        EMITT_RMB(2); break;
      case 0x37: // RMB3 ZZ
        EMITT_RMB(3); break;
      case 0x47: // RMB4 ZZ
        EMITT_RMB(4); break;
      case 0x57: // RMB5 ZZ
        EMITT_RMB(5); break;
      case 0x67: // RMB6 ZZ
        EMITT_RMB(6); break;
      case 0x77: // RMB7 ZZ
        EMITT_RMB(7); break;
///////////////////////////////////////////////////////////////////////////////
// ROL
///////////////////////////////////////////////////////////////////////////////
#define EMITT_ROL(reg) do { \
  EMIT_MOV_R_R(JREG_CY, JREG_TMP2); \
  EMITM_FLAG_C_FROM_N(reg); \
  EMIT_SHL_I_R(1, reg); \
  EMIT_OR_R_R(JREG_TMP2, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0x26: // ROL zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_ROL(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0x36: // ROL zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_ROL(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x2E: // ROL hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_ROL(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0x3E: // ROL hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_ROL(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x2A: // ROL A
        EMITT_ROL(JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// ROR
///////////////////////////////////////////////////////////////////////////////
#define EMITT_ROR(reg) do { \
  EMIT_ANDI_I_R_R(0xFF, reg, reg); \
  EMIT_MOV_R_R(JREG_CY, JREG_TMP2); \
  EMIT_SHL_I_R(7, JREG_TMP2); \
  EMITM_FLAG_C_FROM_B0(reg); \
  EMIT_SHR_I_R(1, reg); \
  EMIT_OR_R_R(JREG_TMP2, reg); \
  EMITM_FLAG_NZ(reg); \
} while(0)
      case 0x66: // ROR zz
        EMITM_READ_ZP(tmp8[0], JREG_TMP0);
        EMITT_ROR(JREG_TMP0);
        EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]);
        break;
      case 0x76: // ROR zz, X
        EMITM_READ_ZP_X(tmp8[0], JREG_TMP0, JREG_TMP1);
        EMITT_ROR(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x6E: // ROR hhll
        EMITM_READ_ABS(tmp16[0], JREG_TMP0);
        EMITT_ROR(JREG_TMP0);
        EMIT_STB_MEMI_R(JREG_TMP0, tmp16[0]);
        break;
      case 0x7E: // ROR hhll, X
        EMITM_READ_ABS_X(tmp16[0], JREG_TMP0, JREG_TMP1);
        EMITT_ROR(JREG_TMP0);
        EMIT_STB_MEMR_R(JREG_TMP0, JREG_TMP1);
        break;
      case 0x6A: // ROR A
        EMITT_ROR(JREG_A);
        break;
///////////////////////////////////////////////////////////////////////////////
// RTI
///////////////////////////////////////////////////////////////////////////////
      case 0x40: // RTI
        // TODO: Fuck you.
        break;
///////////////////////////////////////////////////////////////////////////////
// RTS
///////////////////////////////////////////////////////////////////////////////
      case 0x60: // RTS
        // TODO: Fuck you.
        break;
///////////////////////////////////////////////////////////////////////////////
// SAX/SAY/SXY
///////////////////////////////////////////////////////////////////////////////
      case 0x22: // SAX
        EMIT_MOV_R_R(JREG_A, JREG_TMP0);
        EMIT_MOV_R_R(JREG_X, JREG_A);
        EMIT_MOV_R_R(JREG_TMP0, JREG_X);
        break;
      case 0x42: // SAY
        EMIT_MOV_R_R(JREG_A, JREG_TMP0);
        EMIT_MOV_R_R(JREG_Y, JREG_A);
        EMIT_MOV_R_R(JREG_TMP0, JREG_Y);
        break;
      case 0x02: // SXY
        EMIT_MOV_R_R(JREG_Y, JREG_TMP0);
        EMIT_MOV_R_R(JREG_X, JREG_Y);
        EMIT_MOV_R_R(JREG_TMP0, JREG_X);
        break;
///////////////////////////////////////////////////////////////////////////////
// SBC
///////////////////////////////////////////////////////////////////////////////
#define EMITT_SBC_I_IMPL(reg, dst, use_cy) do { \
  if(use_cy) { \
  } \
} while(0)
#define EMITT_SBC_IMPL(reg, dst, use_cy) do { \
  if(use_cy) { \
  } \
} while(0)
#define EMITT_SBC_I(reg) EMITT_SBC_I_IMPL(reg, reg, 1)
#define EMITT_SBC(reg) EMITT_SBC_IMPL(reg, reg, 1)
      case 0xE9: // SBC #nn
        tmp8[0] = g_cpurom[pc++]; EMITT_SBC_I(JREG_A); break;
      case 0xE5: // SBC zz
        EMITT_READ_ZP();          EMITT_SBC(JREG_A);  break;
      case 0xF5: // SBC zz, X
        EMITT_READ_ZP_X();        EMITT_SBC(JREG_A);  break;
      case 0xF2: // SBC (zz)
        EMITT_READ_IND();         EMITT_SBC(JREG_A);  break;
      case 0xE1: // SBC (zz, X)
        EMITT_READ_IDX_IND();     EMITT_SBC(JREG_A);  break;
      case 0xF1: // SBC (zz), Y
        EMITT_READ_IND_IDX();     EMITT_CMP(JREG_A);  break;
      case 0xED: // SBC hhll
        EMITT_READ_ABS();         EMITT_CMP(JREG_A);  break;
      case 0xFD: // SBC hhll, X
        EMITT_READ_ABS_X();       EMITT_CMP(JREG_A);  break;
      case 0xF9: // SBC hhll, Y
        EMITT_READ_ABS_Y();       EMITT_CMP(JREG_A);  break;
///////////////////////////////////////////////////////////////////////////////
// SEC
///////////////////////////////////////////////////////////////////////////////
      case 0x38: // SEC
        EMIT_MOV_I_R(1, JREG_CY);
        break;
///////////////////////////////////////////////////////////////////////////////
// SED
///////////////////////////////////////////////////////////////////////////////
      case 0xF8: // SED
        // TODO: Fuck you.
        EMIT_ORI_I_R_R(JFLAG_D, JREG_P, JREG_P);
        break;
///////////////////////////////////////////////////////////////////////////////
// SEI
///////////////////////////////////////////////////////////////////////////////
      case 0x78: // SEI
        // TODO: Just write the value to g_cpu.
        EMIT_ORI_I_R_R(JFLAG_I, JREG_P, JREG_P);
        break;
///////////////////////////////////////////////////////////////////////////////
// SET
///////////////////////////////////////////////////////////////////////////////
      case 0xF4: // SET
        // TODO: Fuck you.
        break;
///////////////////////////////////////////////////////////////////////////////
// SMBi
///////////////////////////////////////////////////////////////////////////////
#define EMITT_SMB(bit) do { \
  EMITM_READ_ZP(tmp8[0], JREG_TMP0); \
  EMITM_ORI_I_R_R(1 << bit, JREG_TMP0, JREG_TMP0); \
  EMIT_STB_RAMI_R(JREG_TMP0, tmp8[0]); \
} while(0)
      case 0x87: // SMB0 ZZ
        EMITT_SMB(0); break;
      case 0x97: // SMB1 ZZ
        EMITT_SMB(1); break;
      case 0xA7: // SMB2 ZZ
        EMITT_SMB(2); break;
      case 0xB7: // SMB3 ZZ
        EMITT_SMB(3); break;
      case 0xC7: // SMB4 ZZ
        EMITT_SMB(4); break;
      case 0xD7: // SMB5 ZZ
        EMITT_SMB(5); break;
      case 0xE7: // SMB6 ZZ
        EMITT_SMB(6); break;
      case 0xF7: // SMB7 ZZ
        EMITT_SMB(7); break;
///////////////////////////////////////////////////////////////////////////////
// ST0/ST1/ST2
///////////////////////////////////////////////////////////////////////////////
      case 0x03: // ST0 #nn
        tmp8[0] = g_cpurom[pc++];
        mem_w8_hw(0x1FE000, tmp8[0]);
        break;
      case 0x13: // ST1 #nn
        tmp8[0] = g_cpurom[pc++];
        mem_w8_hw(0x1FE002, tmp8[0]);
        break;
      case 0x23: // ST2 #nn
        tmp8[0] = g_cpurom[pc++];
        mem_w8_hw(0x1FE003, tmp8[0]);
        break;
///////////////////////////////////////////////////////////////////////////////
// STA
///////////////////////////////////////////////////////////////////////////////
      case 0x85: // STA zz
        EMITM_WRITE_ZP(tmp8[0], JREG_A);
        break;
      case 0x95: // STA zz, X
        EMITM_WRITE_ZP_X(tmp8[0], JREG_A, JREG_TMP0);
        break;
      case 0x92: // STA (zz)
        EMITM_WRITE_IND(tmp8[0], JREG_A, JREG_TMP0);
        break;
      case 0x81: // STA (zz, X)
        EMITM_WRITE_IDX_IND(tmp8[0], JREG_A, JREG_TMP0);
        break;
      case 0x91: // STA (zz), Y
        EMITM_WRITE_IND_IDX(tmp8[0], JREG_A, JREG_TMP0);
        break;
      case 0x8D: // STA hhll
        EMITM_WRITE_ABS(tmp8[0], JREG_A);
        break;
      case 0x9D: // STA hhll, X
        EMITM_WRITE_ABS_X(tmp8[0], JREG_A, JREG_TMP0);
        break;
      case 0x99: // STA hhll, Y
        EMITM_WRITE_ABS_Y(tmp8[0], JREG_A, JREG_TMP0);
        break;
///////////////////////////////////////////////////////////////////////////////
// STX
///////////////////////////////////////////////////////////////////////////////
      case 0x86: // STX zz
        EMITM_WRITE_ZP(tmp8[0], JREG_X);
        break;
      case 0x96: // STX zz, Y
        EMITM_WRITE_ZP_Y(tmp8[0], JREG_X, JREG_TMP0);
        break;
      case 0x8E: // STX hhll
        EMITM_WRITE_ABS(tmp8[0], JREG_X);
        break;
///////////////////////////////////////////////////////////////////////////////
// STY
///////////////////////////////////////////////////////////////////////////////
      case 0x84: // STY zz
        EMITM_WRITE_ZP(tmp8[0], JREG_Y);
        break;
      case 0x94: // STY zz, X
        EMITM_WRITE_ZP_X(tmp8[0], JREG_Y, JREG_TMP0);
        break;
      case 0x8C: // STY hhll
        EMITM_WRITE_ABS(tmp8[0], JREG_Y);
        break;
///////////////////////////////////////////////////////////////////////////////
// STZ
///////////////////////////////////////////////////////////////////////////////
      case 0x64: // STZ zz
        EMITM_WRITE_ZP(tmp8[0], 0);
        break;
      case 0x74: // STY zz, X
        EMITM_WRITE_ZP_X(tmp8[0], 0, JREG_TMP0);
        break;
      case 0x9C: // STZ hhll
        EMITM_WRITE_ABS(tmp8[0], 0);
        break;
      case 0x9E: // STZ hhll, X
        EMITM_WRITE_ABS_X(tmp8[0], 0, JREG_TMP0);
        break;
///////////////////////////////////////////////////////////////////////////////
// Txx (HuC6280 transfer)
///////////////////////////////////////////////////////////////////////////////
      case 0xF3: // TAI SHSL, DHDL, LHLL
        // TODO: Fuck you.
        break;
      case 0xE3: // TIA SHSL, DHDL, LHLL
        // TODO: Fuck you.
        break;
      case 0xD3: // TIN SHSL, DHDL, LHLL
        // TODO: Fuck you.
        break;
      case 0xC3: // TDD SHSL, DHDL, LHLL
        // TODO: Fuck you.
        break;
      case 0x73: // TII SHSL, DHDL, LHLL
        // TODO: Fuck you.
        break;
///////////////////////////////////////////////////////////////////////////////
// TAMi
///////////////////////////////////////////////////////////////////////////////
      case 0x53: // TAM #nn
        tmp8[0] = g_cpurom[pc++];
        for(u8 i = 0; i < 8; i++) {
          if(tmp8[0] & (1 << i)) {
            // TODO: emit mem_setmpr(i, JREG_A);
            break;
          }
        }
        break;
///////////////////////////////////////////////////////////////////////////////
// TMAi
///////////////////////////////////////////////////////////////////////////////
      case 0x43: // TMA #nn
        tmp8[0] = g_cpurom[pc++];
        for(u8 i = 7; i >= 0; i--) {
          if(tmp8[0] & (1 << i)) {
            // TODO: emit JREG_A = mem_getmpr(i);
            break;
          }
        }
        break;
///////////////////////////////////////////////////////////////////////////////
// TAX/TAY/TSX/TXA/TXS
///////////////////////////////////////////////////////////////////////////////
      case 0xAA: // TAX
        EMIT_MOV_R_R(JREG_A, JREG_X);
        break;
      case 0xA8: // TAY
        EMIT_MOV_R_R(JREG_A, JREG_Y);
        break;
      case 0xBA: // TSX
        EMIT_MOV_R_R(JREG_S, JREG_X);
        break;
      case 0x8A: // TXA
        EMIT_MOV_R_R(JREG_X, JREG_A);
        break;
      case 0x9A: // TXS
        EMIT_MOV_R_R(JREG_X, JREG_S);
        break;
///////////////////////////////////////////////////////////////////////////////
// TRB
///////////////////////////////////////////////////////////////////////////////
#define EMITT_TRB(func) do { \
  EMIT_MOV_R_R(JREG_TMP0, JREG_TMP1); \
  EMITT_BIT(); \
  EMIT_XORI_I_R_R(0xFF, JREG_A, JREG_TMP0); \
  EMIT_AND_R_R(JREG_TMP0, JREG_TMP1); \
} while(0)
      case 0x14: // TRB zz
        EMITT_READ_ZP();    EMITT_TRB();  EMITT_WRITE_ZP(JREG_TMP1);  break;
      case 0x1C: // TRB hhll
        EMITT_READ_ABS();   EMITT_TRB();  EMITT_WRITE_ABS(JREG_TMP1); break;
///////////////////////////////////////////////////////////////////////////////
// TSB
///////////////////////////////////////////////////////////////////////////////
#define EMITT_TSB(func) do { \
  EMIT_MOV_R_R(JREG_TMP0, JREG_TMP1); \
  EMITT_BIT(); \
  EMIT_OR_R_R(JREG_A, JREG_TMP1); \
} while(0)
      case 0x04: // TSB zz
        EMITT_READ_ZP();    EMITT_TSB();  EMITT_WRITE_ZP(JREG_TMP1);  break;
      case 0x0C: // TSB hhll
        EMITT_READ_ABS();   EMITT_TSB();  EMITT_WRITE_ABS(JREG_TMP1); break;
  }
  return pc - opc;
}
