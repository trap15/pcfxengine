#ifndef CPU_JIT_ALLOC_H_
#define CPU_JIT_ALLOC_H_

void jit_alloc_init(void *buf, u32 size);
void *jit_alloc(u32 size);
void jit_free(void *ptr);

#endif
