#ifndef CPU_JIT_COMPILER_H_
#define CPU_JIT_COMPILER_H_

u32 jit_compiler(u32 pc, u16 *buf, u32 max_insns);

#endif
