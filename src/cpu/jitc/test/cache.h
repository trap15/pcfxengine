#ifndef CPU_JIT_CACHE_H_
#define CPU_JIT_CACHE_H_

void jit_cache_init(void);
void *jit_cache_get_block(u32 pc);

#endif
