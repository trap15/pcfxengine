#include "top.h"

#include "cpu/cpu.h"
#include "cpu/jitc/compiler.h"
#include "cpu/jitc/cache.h"

/* Notes on cache sizing:
 *
 * Too many entries makes cache misses exceptionally painful.
 * Too many instructions makes for more cache waste.
 * Too few instructions means using additional entries for each block.
 * Too few entries means severe cache thrashing.
 *
 * 1 8-bit 6502 instruction ends up at 2-8 16-bit V810 instructions.
 *
 * It's probably worth profiling games to see how big their blocks are, and
 * optimizing based on that information. It may end up that different games
 * _require_ specific tunings, in which case woohoo it's shit.
 */

#define CACHE_ENTRY_COUNT 256
#define CACHE_INSTRUCTION_COUNT 1024
// 512KB (256 entries of 1024 16-bit insns) for JIT instruction cache

// TODO: Probably want to do some sort of hash map for the entries...

typedef struct {
  u32 pc;
  u32 size;
} CacheEntry;

typedef struct {
  u16 insns[CACHE_INSTRUCTION_COUNT];
} CacheData;

static CacheEntry s_entries[CACHE_ENTRY_COUNT];
static CacheData s_data[CACHE_ENTRY_COUNT];
// [0] is most recently used, [CACHE_ENTRY_COUNT-1] is least recently used
static u32 s_entry_order[CACHE_ENTRY_COUNT];

void jit_cache_init(void) {
  for(int i = 0; i < CACHE_ENTRY_COUNT; i++) {
    s_entries[i].pc = ~0;
    s_entries[i].size = 0;
    s_entry_order[i] = i;
  }
}

CacheEntry *jit_cache_add(u32 pc) {
  u32 ent_idx = s_entry_order[CACHE_ENTRY_COUNT - 1];
  CacheEntry *ent = &s_entries[ent_idx];
  CacheData *data = &s_data[ent_idx];
  ent->pc = pc;
  ent->size = jit_compiler(pc, data->insns, CACHE_INSTRUCTION_COUNT);
  return CACHE_ENTRY_COUNT - 1;
}

void jit_cache_advance(int idx) {
  if(idx > 0) {
    // Move the entry forward in the LRU
    s_entry_order[idx-1] ^= s_entry_order[idx];
    s_entry_order[idx] ^= s_entry_order[idx-1];
    s_entry_order[idx-1] ^= s_entry_order[idx];
  }
}

void *jit_cache_get_block(u32 pc) {
  int ent_idx;
  for(ent_idx = 0; ent_idx < CACHE_ENTRY_COUNT; ent_idx++) {
    CacheEntry *cur_ent = &s_entries[s_entry_order[ent_idx]];
    if(cur_ent->pc == pc) {
      break;
    }
    // TODO: Allow jumping inside blocks to be optimal and good
  }
  if(ent_idx == CACHE_ENTRY_COUNT) {
    ent_idx = jit_cache_add(pc);
  }else{
    jit_cache_advance(ent_idx);
    ent_idx = s_entry_order[ent_idx];
  }
  return s_data[ent_idx]->insns;
}
