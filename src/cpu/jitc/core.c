#include "top.h"

#include "cpu/cpu.h"
#include "cpu/jitc/core.h"

CpuState g_cpu;

void cpucore_init(void)
{
}

#include "cpu/jitc/prims.inc.c"

void cpu_run_one(u8 insn)
{
  int i;
  s8 disp;
  u32 param = 0;
  u16 adr;
  u8 tst_nn;
  u8 base;
  u32 res;
  u8 tpfx_saved_a = 0;
  u16 blk_src, blk_dst, blk_len;
  switch(insn) {
#include "cpu/jitc/ops.inc.c"
    default: /* BAD */
      break;
  }
  g_cpu.p &= ~PFLG_T;
_no_T_reset:
  return;
}

void cpu_do_irq(void)
{
  int i;
  int pend = g_cpu.irq_pend & ~g_cpu.irq_mask;
  for(i = 0; i < 3; i++) {
    if(pend & (1 << i)) {
      cpu_irq(CPUV_IRQ2+(i<<1));
      break;
    }
  }
}

s32 cpu_run(s32 cycles)
{
  cpujit_begin();
  /* We should never reach here anyways */
  return cycles;
}

void cpu_run_once(void)
{
  // broken
}

/* Push a byte */
void cpu_push(u8 v)
{
  mem_w8(g_cpu.s | 0x2100, v);
  g_cpu.s--;
}

/* Pop a byte */
u8 cpu_pop(void)
{
  g_cpu.s++;
  return mem_r8(g_cpu.s | 0x2100);
}
