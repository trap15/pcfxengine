	/* r29 = PC */
	/* r28 = S */
	/* r27 = P */
	/* r26 = A */
	/* r25 = X */
	/* r24 = Y */
	/* r23 = param */
	/* r22 = adr */
	/* r21 = tables */
	/* r5 = g_cpu */

#define ERG_PC		r29
#define ERG_S		r28
#define ERG_P		r27
#define ERG_A		r26
#define ERG_X		r25
#define ERG_Y		r24
#define ERG_PARAM	r23
#define ERG_ADR		r22
#define ERG_TBL		r21
#define ERG_CPU		r5

	.section .text
	.align	4

	.global	_cpujit_begin

/* void cpujit_begin(void) */
_cpujit_begin:
	addi	-84, sp, sp
	st.w	lp, 44[sp]
	st.w	r29, 48[sp]
	st.w	r28, 52[sp]
	st.w	r27, 56[sp]
	st.w	r26, 60[sp]
	st.w	r25, 64[sp]
	st.w	r24, 68[sp]
	st.w	r23, 72[sp]
	st.w	r22, 76[sp]
	st.w	r21, 80[sp]

	jal	_jit_setup

	/* load registers from state */
	movhi	hi(_g_cpu), r0, r1
	movea	lo(_g_cpu), r1, ERG_CPU
	ld.w	0x04[ERG_CPU], ERG_PC
	ld.h	0x08[ERG_CPU], ERG_S
	ld.b	0x0B[ERG_CPU], ERG_P
	ld.b	0x0A[ERG_CPU], ERG_A
	ld.b	0x0C[ERG_CPU], ERG_X
	ld.b	0x0D[ERG_CPU], ERG_Y

	andi	0xFF, ERG_P, ERG_P
	andi	0xFF, ERG_A, ERG_A
	andi	0xFF, ERG_X, ERG_X
	andi	0xFF, ERG_Y, ERG_Y

/* TODO */
	mov	ERG_PC, r6
	jal	_jit_get_block
	jmp	[r10]

cpu_run_done:
	/* store registers to state */
	st.h	ERG_PC, 0x04[ERG_CPU]
	st.b	ERG_S, 0x08[ERG_CPU]
	st.b	ERG_P, 0x0B[ERG_CPU]
	st.b	ERG_A, 0x0A[ERG_CPU]
	st.b	ERG_X, 0x0C[ERG_CPU]
	st.b	ERG_Y, 0x0D[ERG_CPU]

	/* We're done, leave. */
	ld.w	80[sp], r21
	ld.w	76[sp], r22
	ld.w	72[sp], r23
	ld.w	68[sp], r24
	ld.w	64[sp], r25
	ld.w	60[sp], r26
	ld.w	56[sp], r27
	ld.w	52[sp], r28
	ld.w	48[sp], r29
	ld.w	44[sp], lp
	addi	84, sp, sp
	jmp	[lp]

_cpu_push_int:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	mov	r6, r7
	mov	ERG_S, r6
	jal	_mem_w8
	add	-1, ERG_S
	st.b	ERG_S, 0x08[ERG_CPU]
	ld.h	0x08[ERG_CPU], ERG_S
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_pop_int:
	addi	-4, sp, sp
	st.w	lp, 0[sp]
	add	1, ERG_S
	st.b	ERG_S, 0x08[ERG_CPU]
	ld.h	0x08[ERG_CPU], ERG_S
	mov	ERG_S, r6
	jal	_mem_r8
	andi	0xFF, r10, r10
	ld.w	0[sp], lp
	addi	4, sp, sp
	jmp	[lp]

_cpu_irq_int:
	addi	-8, sp, sp
	st.w	lp, 0[sp]
	st.h	r6, 4[sp]
	st.h	r0, 6[sp]
	mov	ERG_PC, r6
	shr	8, r6
	jal	_cpu_push_int
	andi	0xFF, ERG_PC, r6
	jal	_cpu_push_int
	andi	~PFLG_B, ERG_P, r6
	jal	_cpu_push_int
	ld.w	4[sp], r6
	addi	1, r6, r7
	st.h	r7, 4[sp]
	jal	_mem_r8
	andi	0xFF, r10, ERG_PC
	ld.w	4[sp], r6
	jal	_mem_r8
	shl	8, r10
	or	r10, ERG_PC
	andi	~(PFLG_D|PFLG_T), ERG_P, ERG_P
	ori	PFLG_I, ERG_P, ERG_P
	andi	0xFFFF, ERG_PC, ERG_PC
	ld.w	0[sp], lp
	addi	8, sp, sp
	jmp	[lp]

_cpu_makecall_int:
	addi	-8, sp, sp
	st.w	lp, 0[sp]
	st.h	r6, 4[sp]
	st.h	r0, 6[sp]
	add	-1, ERG_PC
	mov	ERG_PC, r6
	shr	8, r6
	jal	_cpu_push_int
	andi	0xFF, ERG_PC, r6
	jal	_cpu_push_int
	ld.w	4[sp], ERG_PC
	ld.w	0[sp], lp
	addi	8, sp, sp
	jmp	[lp]
