/* Opcode primitives */

/*********************************************/
/**************** MISC. PRIMS ****************/
/*********************************************/
#define PRIM_ZN(V) \
  g_cpu.p |= cpu_zn_tbl[(V) & 0xFF];
#define PRIM_ZN_MASK(V) \
  g_cpu.p &= ~(PFLG_Z|PFLG_N); \
  PRIM_ZN(V)

#define ZP_BASE 0x2000

#define PFX_T \
  if(g_cpu.p & PFLG_T) { \
    tpfx_saved_a = g_cpu.a; \
    g_cpu.a = mem_r8(ZP_BASE | g_cpu.x); \
  }
#define SFX_T \
  if(g_cpu.p & PFLG_T) { \
    mem_w8(ZP_BASE | g_cpu.x, g_cpu.a); \
    g_cpu.a = tpfx_saved_a; \
  }

/********************************************************/
/**************** ADDRESS MODE RAW PRIMS ****************/
/********************************************************/
#define ADDR_REL \
  disp = mem_fetch8(g_cpu.pc++); \
  adr = g_cpu.pc + disp;

#define ADDR_ZP \
  adr = ZP_BASE | mem_fetch8(g_cpu.pc++);

#define ADDR_ZPX \
  adr = ZP_BASE | ((g_cpu.x + mem_fetch8(g_cpu.pc++)) & 0xFF);

#define ADDR_ZPY \
  adr = ZP_BASE | ((g_cpu.y + mem_fetch8(g_cpu.pc++)) & 0xFF);

#define ADDR_ZPIND \
  base = mem_fetch8(g_cpu.pc++); \
  adr = mem_r8(ZP_BASE | base); \
  base++; \
  adr |= mem_r8(ZP_BASE | base) << 8;

#define ADDR_AB \
  adr = mem_fetch8(g_cpu.pc++); \
  adr |= mem_fetch8(g_cpu.pc++) << 8;

#define ADDR_ABX \
  adr = mem_fetch8(g_cpu.pc++); \
  adr |= mem_fetch8(g_cpu.pc++) << 8; \
  adr += g_cpu.x;

#define ADDR_ABY \
  adr = mem_fetch8(g_cpu.pc++); \
  adr |= mem_fetch8(g_cpu.pc++) << 8; \
  adr += g_cpu.y;

#define ADDR_IX \
  base = mem_fetch8(g_cpu.pc++) + g_cpu.x; \
  adr = mem_r8(ZP_BASE | base); \
  base++; \
  adr |= mem_r8(ZP_BASE | base) << 8;

#define ADDR_IY \
  base = mem_fetch8(g_cpu.pc++); \
  adr = mem_r8(ZP_BASE | base); \
  base++; \
  adr |= mem_r8(ZP_BASE | base) << 8; \
  adr += g_cpu.y;

#define PRD_IM param = mem_fetch8(g_cpu.pc++);
#define PRD_A  param = g_cpu.a;
#define PWR_A  g_cpu.a = param;
#define PRD_TST_NN tst_nn = mem_fetch8(g_cpu.pc++);
#define PRD_ABADR ADDR_AB param = adr;
#define PRD_REL ADDR_REL param = adr;

/****************************************************/
/**************** ADDRESS MODE PRIMS ****************/
/****************************************************/
#define ARD_PFX param = mem_r8(adr);
#define ARD_SFX

#define AWR_PFX
#define AWR_SFX mem_w8(adr, param);
#define ARW_PFX param = mem_r8(adr);
#define ARW_SFX mem_w8(adr, param);
#define ARD16_PFX param = mem_r8(adr) | (mem_r8(adr+1) << 8);
#define ARD16_SFX

/* IM PRIMS */
#define ARD_IM(OP) PRD_IM OP
/* A PRIMS */
#define ARD_A(OP) PRD_A OP
#define AWR_A(OP) OP PWR_A
#define ARW_A(OP) PRD_A OP PWR_A
/* REL PRIMS */
#define ARD_REL(OP) PRD_REL OP
/* ZP PRIMS */
#define ARD_ZP(OP) ADDR_ZP ARD_PFX OP ARD_SFX
#define AWR_ZP(OP) ADDR_ZP AWR_PFX OP AWR_SFX
#define ARW_ZP(OP) ADDR_ZP ARW_PFX OP ARW_SFX
/* ZPX PRIMS */
#define ARD_ZPX(OP) ADDR_ZPX ARD_PFX OP ARD_SFX
#define AWR_ZPX(OP) ADDR_ZPX AWR_PFX OP AWR_SFX
#define ARW_ZPX(OP) ADDR_ZPX ARW_PFX OP ARW_SFX
/* ZPY PRIMS */
#define ARD_ZPY(OP) ADDR_ZPY ARD_PFX OP ARD_SFX
#define AWR_ZPY(OP) ADDR_ZPY AWR_PFX OP AWR_SFX
#define ARW_ZPY(OP) ADDR_ZPY ARW_PFX OP ARW_SFX
/* ZPIND PRIMS */
#define ARD_ZPIND(OP) ADDR_ZPIND ARD_PFX OP ARD_SFX
#define AWR_ZPIND(OP) ADDR_ZPIND AWR_PFX OP AWR_SFX
#define ARW_ZPIND(OP) ADDR_ZPIND ARW_PFX OP ARW_SFX
/* AB PRIMS */
#define ARD_AB(OP) ADDR_AB ARD_PFX OP ARD_SFX
#define AWR_AB(OP) ADDR_AB AWR_PFX OP AWR_SFX
#define ARW_AB(OP) ADDR_AB ARW_PFX OP ARW_SFX
/* ABX PRIMS */
#define ARD_ABX(OP) ADDR_ABX ARD_PFX OP ARD_SFX
#define AWR_ABX(OP) ADDR_ABX AWR_PFX OP AWR_SFX
#define ARW_ABX(OP) ADDR_ABX ARW_PFX OP ARW_SFX
/* ABY PRIMS */
#define ARD_ABY(OP) ADDR_ABY ARD_PFX OP ARD_SFX
#define AWR_ABY(OP) ADDR_ABY AWR_PFX OP AWR_SFX
#define ARW_ABY(OP) ADDR_ABY ARW_PFX OP ARW_SFX
/* IX PRIMS */
#define ARD_IX(OP) ADDR_IX ARD_PFX OP ARD_SFX
#define AWR_IX(OP) ADDR_IX AWR_PFX OP AWR_SFX
#define ARW_IX(OP) ADDR_IX ARW_PFX OP ARW_SFX
/* IY PRIMS */
#define ARD_IY(OP) ADDR_IY ARD_PFX OP ARD_SFX
#define AWR_IY(OP) ADDR_IY AWR_PFX OP AWR_SFX
#define ARW_IY(OP) ADDR_IY ARW_PFX OP ARW_SFX
/* IMZP PRIMS */
#define ARD_IMZP(OP)  PRD_TST_NN ADDR_ZP ARD_PFX OP ARD_SFX
/* IMZPX PRIMS */
#define ARD_IMZPX(OP) PRD_TST_NN ADDR_ZPX ARD_PFX OP ARD_SFX
/* IMAB PRIMS */
#define ARD_IMAB(OP)  PRD_TST_NN ADDR_AB ARD_PFX OP ARD_SFX
/* IMABX PRIMS */
#define ARD_IMABX(OP) PRD_TST_NN ADDR_ABX ARD_PFX OP ARD_SFX
/* ABADR PRIMS */
#define ARD_ABADR(OP) PRD_ABADR OP
/* ABIND PRIMS */
#define ARD_ABIND(OP) ADDR_AB ARD16_PFX OP ARD16_SFX
/* ABINDX PRIMS */
#define ARD_ABINDX(OP) ADDR_ABX ARD16_PFX OP ARD16_SFX
/* BLK PRIMS */
#define ARD_BLK(OP) \
  ADDR_AB blk_src = adr; \
  ADDR_AB blk_dst = adr; \
  ADDR_AB blk_len = adr; \
  OP

/**********************************************/
/**************** OPCODE PRIMS ****************/
/**********************************************/
#define OP_ADC PFX_T \
if(g_cpu.p & PFLG_D) { \
  u32 low; \
  low = (g_cpu.a & 0xF) + (param & 0xF) + (g_cpu.p & PFLG_C); \
  res = (g_cpu.a & 0xF0) + (param & 0xF0); \
  g_cpu.p &= ~(PFLG_N|PFLG_Z|PFLG_C); \
  if(low >= 10) { \
    res += 0x10; \
    low += 6; \
  } \
  if(res >= 0xA0) { \
    res += 0x60; \
  } \
  g_cpu.p |= (res >> 8) & PFLG_C; \
  g_cpu.a = (low & 0xF) | (res & 0xF0); \
}else{ \
  res = g_cpu.a + param + (g_cpu.p & PFLG_C); \
  g_cpu.p &= ~(PFLG_N|PFLG_V|PFLG_Z|PFLG_C); \
  /* Overflow calculation */ \
  g_cpu.p |= (((g_cpu.a ^ param ^ 0x80) & (g_cpu.a ^ res)) >> 1) & PFLG_V; \
  /* Carry calculation */ \
  g_cpu.p |= (res >> 8) & PFLG_C; \
  g_cpu.a = res; \
} \
PRIM_ZN(g_cpu.a) \
SFX_T

#define OP_SBC PFX_T \
if(g_cpu.p & PFLG_D) { \
  u32 low, high; \
  res = g_cpu.a - param - ((g_cpu.p & PFLG_C) ^ PFLG_C); \
  low = (g_cpu.a & 0xF) - (param & 0xF) - ((g_cpu.p & PFLG_C) ^ PFLG_C); \
  high = (g_cpu.a & 0xF0) - (param & 0xF0); \
  g_cpu.p &= ~(PFLG_N|PFLG_Z|PFLG_C); \
  if(low & 0xF0) low -= 6; \
  if(low & 0x80) high -= 0x10; \
  if(high & 0xF00) high -= 0x60; \
  g_cpu.p |= ((res >> 8) & PFLG_C) ^ PFLG_C; \
  g_cpu.a = (low & 0xF) | (high & 0xF0); \
}else{ \
  res = g_cpu.a - param - ((g_cpu.p & PFLG_C) ^ PFLG_C); \
  g_cpu.p &= ~(PFLG_N|PFLG_V|PFLG_Z|PFLG_C); \
  /* Overflow calculation */ \
  g_cpu.p |= (((g_cpu.a ^ param) & (g_cpu.a ^ res)) >> 1) & PFLG_V; \
  /* Carry calculation */ \
  g_cpu.p |= ((res >> 8) & PFLG_C) ^ PFLG_C; \
  g_cpu.a = res; \
} \
PRIM_ZN(g_cpu.a) \
SFX_T

#define OP_CMP_BASE(reg) \
  res = reg - param; \
  g_cpu.p &= ~(PFLG_N|PFLG_V|PFLG_Z|PFLG_C); \
  /* Carry calculation */ \
  g_cpu.p |= ((res >> 8) & PFLG_C) ^ PFLG_C; \
  PRIM_ZN(res) \

#define OP_CMP OP_CMP_BASE(g_cpu.a)
#define OP_CPX OP_CMP_BASE(g_cpu.x)
#define OP_CPY OP_CMP_BASE(g_cpu.y)

#define OP_AND PFX_T g_cpu.a &= param; PRIM_ZN_MASK(g_cpu.a) SFX_T
#define OP_EOR PFX_T g_cpu.a ^= param; PRIM_ZN_MASK(g_cpu.a) SFX_T
#define OP_ORA PFX_T g_cpu.a |= param; PRIM_ZN_MASK(g_cpu.a) SFX_T

#define OP_BIT \
  g_cpu.p &= ~(PFLG_Z|PFLG_V|PFLG_N); \
  if((param & g_cpu.a) == 0) g_cpu.p |= PFLG_Z; \
  g_cpu.p |= param & (PFLG_V|PFLG_N);

#define OP_ASL \
  g_cpu.p &= ~(PFLG_Z|PFLG_N|PFLG_C); \
  g_cpu.p |= (param >> 7) & PFLG_C; \
  param <<= 1; \
  PRIM_ZN(param)

#define OP_LSR \
  g_cpu.p &= ~(PFLG_Z|PFLG_N|PFLG_C); \
  g_cpu.p |= param & PFLG_C; \
  param >>= 1; \
  PRIM_ZN(param)

#define OP_ROL \
  i = param >> 7; \
  param <<= 1; \
  param |= g_cpu.p & PFLG_C; \
  g_cpu.p &= ~(PFLG_Z|PFLG_N|PFLG_C); \
  g_cpu.p |= i & PFLG_C; \
  PRIM_ZN(param)

#define OP_ROR \
  i = param; \
  param >>= 1; \
  param |= (g_cpu.p & PFLG_C) << 7; \
  g_cpu.p &= ~(PFLG_Z|PFLG_N|PFLG_C); \
  g_cpu.p |= i & PFLG_C; \
  PRIM_ZN(param)

#define OP_BBR(I) \
  if(!(param & (1 << I))) { \
    disp = mem_fetch8(g_cpu.pc++); \
    g_cpu.pc += disp; \
  }else \
    g_cpu.pc++;
#define OP_BBR0 OP_BBR(0)
#define OP_BBR1 OP_BBR(1)
#define OP_BBR2 OP_BBR(2)
#define OP_BBR3 OP_BBR(3)
#define OP_BBR4 OP_BBR(4)
#define OP_BBR5 OP_BBR(5)
#define OP_BBR6 OP_BBR(6)
#define OP_BBR7 OP_BBR(7)

#define OP_BBS(I) \
  if(param & (1 << I)) { \
    disp = mem_fetch8(g_cpu.pc++); \
    g_cpu.pc += disp; \
  }else \
    g_cpu.pc++;
#define OP_BBS0 OP_BBS(0)
#define OP_BBS1 OP_BBS(1)
#define OP_BBS2 OP_BBS(2)
#define OP_BBS3 OP_BBS(3)
#define OP_BBS4 OP_BBS(4)
#define OP_BBS5 OP_BBS(5)
#define OP_BBS6 OP_BBS(6)
#define OP_BBS7 OP_BBS(7)

#define OP_Bcond(COND) \
  if(COND) \
    g_cpu.pc = param;

#define OP_BVC OP_Bcond(!(g_cpu.p & PFLG_V))
#define OP_BVS OP_Bcond( (g_cpu.p & PFLG_V))
#define OP_BCC OP_Bcond(!(g_cpu.p & PFLG_C))
#define OP_BCS OP_Bcond( (g_cpu.p & PFLG_C))
#define OP_BNE OP_Bcond(!(g_cpu.p & PFLG_Z))
#define OP_BEQ OP_Bcond( (g_cpu.p & PFLG_Z))
#define OP_BPL OP_Bcond(!(g_cpu.p & PFLG_N))
#define OP_BMI OP_Bcond( (g_cpu.p & PFLG_N))

#define OP_BRA OP_Bcond(1)

#define OP_TAX g_cpu.x = g_cpu.a; PRIM_ZN_MASK(g_cpu.a);
#define OP_TXA g_cpu.a = g_cpu.x; PRIM_ZN_MASK(g_cpu.a);
#define OP_TAY g_cpu.y = g_cpu.a; PRIM_ZN_MASK(g_cpu.a);
#define OP_TYA g_cpu.a = g_cpu.y; PRIM_ZN_MASK(g_cpu.a);
#define OP_TSX g_cpu.x = g_cpu.s;
#define OP_TXS g_cpu.s = g_cpu.x;

#define OP_SAX param = g_cpu.x; g_cpu.x = g_cpu.a; g_cpu.a = param;
#define OP_SAY param = g_cpu.y; g_cpu.y = g_cpu.a; g_cpu.a = param;
#define OP_SXY param = g_cpu.x; g_cpu.x = g_cpu.y; g_cpu.y = param;

#define OP_DEC param--; PRIM_ZN_MASK(param);
#define OP_INC param++; PRIM_ZN_MASK(param);
#define OP_DEX g_cpu.x--; PRIM_ZN_MASK(g_cpu.x);
#define OP_INX g_cpu.x++; PRIM_ZN_MASK(g_cpu.x);
#define OP_DEY g_cpu.y--; PRIM_ZN_MASK(g_cpu.y);
#define OP_INY g_cpu.y++; PRIM_ZN_MASK(g_cpu.y);

#define OP_TST \
  g_cpu.p &= ~(PFLG_Z|PFLG_V|PFLG_N); \
  g_cpu.p |= param & (PFLG_V|PFLG_N); \
  if((param & tst_nn) == 0) \
    g_cpu.p |= PFLG_Z;

#define OP_TMA \
  for(i = 7; i >= 0; i--) { \
    if(param & (1 << i)) { \
	    g_cpu.a = mem_getmpr(i); \
      break; \
    } \
  }

#define OP_TAM \
  for(i = 7; i >= 0; i--) { \
    if(param & (1 << i)) { \
	    mem_setmpr(i, g_cpu.a); \
    } \
  }

#define OP_BSR cpu_makecall(param);

#define OP_CLA g_cpu.a = 0;
#define OP_CLX g_cpu.x = 0;
#define OP_CLY g_cpu.y = 0;
#define OP_CLC g_cpu.p &= ~(PFLG_C);
#define OP_CLD g_cpu.p &= ~(PFLG_D);
#define OP_CLI g_cpu.p &= ~(PFLG_I);
#define OP_CLV g_cpu.p &= ~(PFLG_V);

#define OP_SEC g_cpu.p |= PFLG_C;
#define OP_SED g_cpu.p |= PFLG_D;
#define OP_SEI g_cpu.p |= PFLG_I;
#define OP_SET g_cpu.p |= PFLG_T;

#define OP_CSH g_cpu.speed = 1;
#define OP_CSL g_cpu.speed = 0;

#define OP_TRB \
  g_cpu.p &= ~(PFLG_Z | PFLG_V | PFLG_N); \
  g_cpu.p |= param & (PFLG_N | PFLG_V); \
  param &= ~g_cpu.a; \
  if(param == 0) \
    g_cpu.p |= PFLG_Z;

#define OP_TSB \
  g_cpu.p &= ~(PFLG_Z | PFLG_V | PFLG_N); \
  g_cpu.p |= param & (PFLG_N | PFLG_V); \
  param |= g_cpu.a; \
  if(param == 0) \
    g_cpu.p |= PFLG_Z;

#define OP_SMB(I) param |= 1 << I;
#define OP_SMB0 OP_SMB(0)
#define OP_SMB1 OP_SMB(1)
#define OP_SMB2 OP_SMB(2)
#define OP_SMB3 OP_SMB(3)
#define OP_SMB4 OP_SMB(4)
#define OP_SMB5 OP_SMB(5)
#define OP_SMB6 OP_SMB(6)
#define OP_SMB7 OP_SMB(7)

#define OP_RMB(I) param &= ~(1 << I);
#define OP_RMB0 OP_RMB(0)
#define OP_RMB1 OP_RMB(1)
#define OP_RMB2 OP_RMB(2)
#define OP_RMB3 OP_RMB(3)
#define OP_RMB4 OP_RMB(4)
#define OP_RMB5 OP_RMB(5)
#define OP_RMB6 OP_RMB(6)
#define OP_RMB7 OP_RMB(7)

#define OP_LDA g_cpu.a = param; PRIM_ZN_MASK(g_cpu.a);
#define OP_LDX g_cpu.x = param; PRIM_ZN_MASK(g_cpu.x);
#define OP_LDY g_cpu.y = param; PRIM_ZN_MASK(g_cpu.y);

#define OP_STA param = g_cpu.a;
#define OP_STX param = g_cpu.x;
#define OP_STY param = g_cpu.y;
#define OP_STZ param = 0;

#define OP_RTI \
  g_cpu.p = cpu_pop(); \
  g_cpu.pc = cpu_pop(); \
  g_cpu.pc |= cpu_pop() << 8;
#define OP_RTS \
  g_cpu.pc = cpu_pop(); \
  g_cpu.pc |= cpu_pop() << 8; \
  g_cpu.pc++;

#define OP_PHA cpu_push(g_cpu.a);
#define OP_PHP g_cpu.p &= ~PFLG_T; cpu_push(g_cpu.p | PFLG_B);
#define OP_PHX cpu_push(g_cpu.x);
#define OP_PHY cpu_push(g_cpu.y);
#define OP_PLA g_cpu.a = cpu_pop(); PRIM_ZN_MASK(g_cpu.a);
#define OP_PLP g_cpu.p = cpu_pop();
#define OP_PLX g_cpu.x = cpu_pop(); PRIM_ZN_MASK(g_cpu.x);
#define OP_PLY g_cpu.y = cpu_pop(); PRIM_ZN_MASK(g_cpu.y);

#define OP_JMP g_cpu.pc = param;
#define OP_JSR cpu_makecall(param);

#define OP_ST0 mem_w8_hw(0x1FE000, param);
#define OP_ST1 mem_w8_hw(0x1FE002, param);
#define OP_ST2 mem_w8_hw(0x1FE003, param);

#define OP_TAI \
  i = 0; do { \
    mem_w8(blk_dst++, mem_r8(blk_src + i)); \
    i ^= 1; \
  } while(--blk_len);
#define OP_TDD \
  do { \
    mem_w8(blk_dst--, mem_r8(blk_src--)); \
  } while(--blk_len);
#define OP_TIA \
  i = 0; do { \
    mem_w8(blk_dst + i, mem_r8(blk_src++)); \
    i ^= 1; \
  } while(--blk_len);
#define OP_TII \
  do { \
    mem_w8(blk_dst++, mem_r8(blk_src++)); \
  } while(--blk_len);
#define OP_TIN \
  do { \
    mem_w8(blk_dst, mem_r8(blk_src++)); \
  } while(--blk_len);

#define OP_BRK \
  g_cpu.p &= ~PFLG_T; \
  cpu_irq(CPUV_BRK);

#define OP_NOP
