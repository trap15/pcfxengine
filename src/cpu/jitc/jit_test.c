#include "top.h"

#include "cpu/cpu.h"
#include "cpu/jitc/cache.h"

// Loads g_cpu registers into the JIT registers and starts executing.
void jit_trampoline(void *pc);

void cpucore_init(void) {
  jit_cache_init();
}

s32 cpu_run(s32 cycles) {
  void *func = jit_cache_get_block(g_cpu.pc);
  jit_trampoline(func);
}

void cpu_run_once(void) {
  // nop
}

static void cpu_irq(int vec) {
  // TODO: This needs reworking
  cpu_push(g_cpu.pc >> 8);
  cpu_push(g_cpu.pc >> 0);
  cpu_push(g_cpu.p & ~(PFLG_B));
  g_cpu.p &= ~(PFLG_D|PFLG_T);
  g_cpu.pc = mem_r8(vec) | (mem_r8(vec+1) << 8);
  g_cpu.p |= PFLG_I;
}

static void cpu_do_irq(void) {
  int pend = g_cpu.irq_pend & ~g_cpu.irq_mask;
  for(int i = 0; i < 3; i++) {
    if(pend & (1 << i)) {
      cpu_irq(CPUV_IRQ2 + (i << 1));
      break;
    }
  }
}

// Runs on every basic block invocation
void jit_block_run(void) {
  // Check IRQs and dispatch if necessary.
  if(!(g_cpu.p & PFLG_I) && (g_cpu.irq_pend & ~g_cpu.irq_mask)) {
    cpu_do_irq();
  }
}

void cpu_makecall(u16 npc) {
  // TODO: Write a jsr instruction to a fake RAM and JIT that.
}
