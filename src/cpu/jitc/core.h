#ifndef FXENG_CPUCORE_JITC_H_
#define FXENG_CPUCORE_JITC_H_

void cpujit_begin(void);

void cpu_push_int(u8 v);
u8 cpu_pop_int(void);
void cpu_irq_int(int vec);
void cpu_makecall_int(u16 npc);

#endif
