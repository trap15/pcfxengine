#include "top.h"

void cpu_init(void)
{
  g_cpu.pad1 = 0;
  g_cpu.pad2_fix1 = 0x21;
  cpucore_init();
  cpu_reset();
}

void cpu_reset(void)
{
  g_cpu.pc = mem_r8(CPUV_RESET) | (mem_r8(CPUV_RESET+1) << 8);
  g_cpu.cyc_left = 0;
  g_cpu.p = PFLG_B | PFLG_I;
  g_cpu.speed = 0;
}

#if !CPUCORE_JIT
/* Force an IRQ */
void cpu_irq(int vec)
{
  dbgbrk1(DBGCODE_IRQ, vec);
  cpu_push(g_cpu.pc >> 8);
  cpu_push(g_cpu.pc >> 0);
  cpu_push(g_cpu.p & ~(PFLG_B));
  g_cpu.p &= ~(PFLG_D|PFLG_T);
  g_cpu.pc = mem_r8(vec) | (mem_r8(vec+1) << 8);
  g_cpu.p |= PFLG_I;
}

/* Force a call */
void cpu_makecall(u16 npc)
{
  g_cpu.pc--;
  cpu_push(g_cpu.pc >> 8);
  cpu_push(g_cpu.pc >> 0);
  g_cpu.pc = npc;
}
#endif
