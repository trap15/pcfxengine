#include "top.h"

int g_running;

int main(int argc, char *argv[])
{
  /* Mask off all the IRQs then let the CPU manage interrupts */
  irq_set_mask(0xFF);
  irq_enable();

  /* Basic setup for the VCE */
	eris_tetsu_init();
	eris_tetsu_set_priorities(0, 0, 1, 2, 3, 4, 5);
	eris_tetsu_set_king_palette(KING_PAL_BASE,KING_PAL_BASE,KING_PAL_BASE,KING_PAL_BASE);
  /* Set the 7up palettes */
  out16(VCE_BASE+0, 0x04);
  out16(VCE_BASE+4, 0x8000);

  int i;
  for(i = 0; i < KING_PAL_BASE; i++)
    eris_tetsu_set_palette(i, (i << 12) ^ (i << 8) ^ (i << 1) ^ (i << 4) ^ (i >> 3));

  g_running = 1;

  emu_init();
  ui_init();

  emu_run();
  return 0;
}
