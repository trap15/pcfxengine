#!/usr/bin/env ruby

def yuv_algo(r,g,b)
  y = ( 0.299000 * r) + (0.587000 * g) + (0.114000 * b)
  u = (-0.147138 * r) - (0.288862 * g) + (0.436000 * b) + 0x80
  v = ( 0.615000 * r) - (0.514986 * g) - (0.100014 * b) + 0x80

  y = 0x00 if(y < 0x00)
  y = 0xFF if(y > 0xFF)
  u = 0x00 if(u < 0x00)
  u = 0xFF if(u > 0xFF)
  v = 0x00 if(v < 0x00)
  v = 0xFF if(v > 0xFF)

  u /= 16
  v /= 16
  y = y.round
  u = u.round
  v = v.round

  return y,u,v
end

def rgb2yuv(r,g,b)
  y,u,v = yuv_algo(r*36,g*36,b*36)

  return ((y & 0xFF) << 8) | ((u & 0xF) << 4) | ((v & 0xF) << 0)
end

def gen_rgb2yuv
  print "_rgb2yuv_tbl:\n"
  for i in 0...512
    r = (i >> 3) & 7
    g = (i >> 6) & 7
    b = (i >> 0) & 7
    print "\t.short\t" if (i & 7) == 0
    printf "0x%04X", rgb2yuv(r,g,b)
    print "," if (i & 7) != 7
    print "\n" if (i & 7) == 7
  end
end

gen_rgb2yuv

