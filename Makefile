DEBUG = 0

CPU_CORE       = asm

CD_OBJECTS     =
OBJECTS        = src/main.o src/emu.o \
		src/mem.o src/memintf.o \
		src/rom/hes.o src/rom/rom.o src/rom/tmprom.o \
		src/cpu/cpu.o \
		src/io/ext1.o src/io/ext2.o \
			src/io/intctrl.o src/io/ioprt.o \
			src/io/psg.o src/io/tmr.o \
			src/io/vce.o src/io/vdc.o \
		src/ui/ui.o

ifeq ($(CPU_CORE),c)
CFLAGS        += -DCPUCORE_C=1
OBJECTS       += src/cpu/$(CPU_CORE)/core.o
else ifeq ($(CPU_CORE),asm)
CFLAGS        += -DCPUCORE_ASM=1
OBJECTS       += src/cpu/$(CPU_CORE)/core.o
else ifeq ($(CPU_CORE),jitc)
CFLAGS        += -DCPUCORE_JIT=1
OBJECTS       += src/cpu/$(CPU_CORE)/core.o src/cpu/$(CPU_CORE)/base.o \
		src/cpu/$(CPU_CORE)/jit.o
endif

ifeq ($(DEBUG),1)
CFLAGS        += -O0 -g
ASFLAGS       += -O0 -g
LDFLAGS       += -g
else
CFLAGS        += -O2
ASFLAGS       += -O2
LDFLAGS       += -O2
endif

CFLAGS        += -mno-long-calls -mno-prolog-function
CFLAGS        += -Wall -Werror

ELF_TARGET     = fxeng.elf
BIN_TARGET     = fxeng.bin
ADD_FILES      = 
CDOUT          = fxeng_cd
CFLAGS        += -Isrc/
ASFLAGS       += -Isrc/

TARGETS       += $(ELF_TARGET) $(BIN_TARGET)
PREFIX         = v810-
CC             = $(PREFIX)gcc
AS             = $(PREFIX)gcc
AR             = $(PREFIX)ar
LD             = $(PREFIX)ld
OBJCOPY        = $(PREFIX)objcopy
CFLAGS        += -I$(V810DEV)/include/ -nostdlib -mbig-switch -mv810
LIBS           = -leris
LDFLAGS       += -L$(V810DEV)/lib/ -T$(V810DEV)/v810/lib/ldscripts/v810.x $(V810DEV)/v810/lib/crt0.o

.PHONY: all cd clean install .FORCE

all: $(OBJECTS) $(TARGETS)

$(CD_OBJECTS): .FORCE lbas.h

src/cpu/c/core.o: src/cpu/c/core.c src/cpu/c/ops.inc.c src/cpu/c/prims.inc.c

%.o: %.S
	$(AS) $(ASFLAGS) $< -c -o $@
%.o: %.s
	$(AS) $(ASFLAGS) $< -c -o $@
%.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@
%.elf: $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@ 
%.bin: %.elf
	$(OBJCOPY) -O binary $< $@
cd: $(TARGETS)
	bincat out.bin lbas.h $(BIN_TARGET) $(ADD_FILES)
	make cdclean -C .
	make all -C .
	bincat out.bin lbas.h $(BIN_TARGET) $(ADD_FILES)
	pcfx-cdlink cdlink.txt $(CDOUT)

lbas.h:
	bincat - lbas.h $(BIN_TARGET) $(ADD_FILES)

clean:
	rm -rf $(OBJECTS) $(TARGETS) lbas.h $(CDOUT).cue $(CDOUT).bin

cdclean:
	rm -rf $(OBJECTS) $(TARGETS) $(CDOUT).cue $(CDOUT).bin
